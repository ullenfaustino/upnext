-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2017 at 11:42 AM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upnext`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_control`
--

CREATE TABLE IF NOT EXISTS `admin_control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_control`
--

INSERT INTO `admin_control` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'member_registration', '0', '2015-08-17 06:06:59', '2015-08-17 21:06:59'),
(2, 'member_payout', '0', '2015-08-25 07:32:19', '2015-08-25 22:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` text,
  `bank_account_no` varchar(500) DEFAULT NULL,
  `bank_account_name` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `bank_account_no`, `bank_account_name`, `created_at`, `updated_at`) VALUES
(1, 'Bank of the Philippines Island', '123456789876', 'Lorem Ipsum Dolor Soene', '2017-06-11 06:35:50', NULL),
(2, 'Security Bank', '987654321', 'Lorem Ipsum Dolor Soene', '2017-06-11 06:35:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE IF NOT EXISTS `bulletin` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` int(8) DEFAULT '80',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE IF NOT EXISTS `codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `generated_code` varchar(45) DEFAULT NULL,
  `purchase_activation_id` int(11) DEFAULT NULL,
  `user_id` int(8) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `generated_code`, `purchase_activation_id`, `user_id`, `is_used`, `created_at`, `updated_at`) VALUES
(1, '2017NZ1ds5-0613F4Cwrm-191454GZGK', 1, 7, 1, '2017-06-13 19:14:54', '2017-06-13 19:19:52'),
(2, '20170E30Ja-0613hTgeud-192123KG6N', 2, 8, 1, '2017-06-13 19:21:23', '2017-06-13 19:22:22'),
(3, '2017U2zi9p-0613RxhAj6-1921234J02', 2, 9, 1, '2017-06-13 19:21:23', '2017-06-13 19:24:11'),
(4, '20176NUD7e-0613id5Ypw-1921236ES6', 2, 10, 1, '2017-06-13 19:21:23', '2017-06-13 19:26:11'),
(5, '2017l2uYwW-0613T163F5-1921236C6A', 2, 11, 1, '2017-06-13 19:21:23', '2017-06-13 19:27:55'),
(6, '2017Md2Lm8-0613ICmJRj-192123TEQN', 2, 12, 1, '2017-06-13 19:21:23', '2017-06-13 19:32:42'),
(7, '2017pWz8pj-0613sDNSb5-192123W3EB', 2, 0, 0, '2017-06-13 19:21:23', '2017-06-13 19:21:23'),
(8, '2017NgeIQB-0613Kk6INp-192123MN2H', 2, 13, 1, '2017-06-13 19:21:23', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `company_allocation`
--

CREATE TABLE IF NOT EXISTS `company_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_cost` decimal(12,2) DEFAULT '160.00',
  `operating_exp` decimal(12,2) DEFAULT '50.00',
  `incentives` decimal(12,2) DEFAULT '20.00',
  `misc` decimal(12,2) DEFAULT '20.00',
  `direct` decimal(12,2) DEFAULT '100.00',
  `matrix` decimal(12,2) DEFAULT '980.00',
  `company_a_profit` decimal(12,2) DEFAULT '85.00',
  `company_b_profit` decimal(12,2) DEFAULT '85.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `company_allocation`
--

INSERT INTO `company_allocation` (`id`, `user_id`, `product_cost`, `operating_exp`, `incentives`, `misc`, `direct`, `matrix`, `company_a_profit`, `company_b_profit`, `created_at`, `updated_at`) VALUES
(1, 7, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:19:52', '2017-06-13 19:19:52'),
(2, 8, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:22:22', '2017-06-13 19:22:22'),
(3, 9, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:24:11', '2017-06-13 19:24:11'),
(4, 10, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:26:11', '2017-06-13 19:26:11'),
(5, 11, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:27:55', '2017-06-13 19:27:55'),
(6, 12, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:32:42', '2017-06-13 19:32:42'),
(7, 13, 160.00, 50.00, 20.00, 20.00, 100.00, 980.00, 85.00, 85.00, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `company_pullouts`
--

CREATE TABLE IF NOT EXISTS `company_pullouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alloc_name` varchar(500) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT '0.00',
  `remarks` text,
  `admin_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE IF NOT EXISTS `constants` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '""',
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'entry_amount', '12000', '2015-05-12 15:00:06', '2015-05-12 15:00:06'),
(2, 'direct_referral_bonus', '0', '2015-05-17 14:51:19', '2015-05-17 14:51:19'),
(3, 'exit_bonus', '36000', '2015-05-18 19:56:52', '2015-05-18 19:56:52'),
(4, 'misc', '0', '2015-05-18 19:57:14', '2015-05-18 19:57:14'),
(5, 'conceptualization_fee', '0', '2015-05-18 19:57:42', '2015-05-18 19:57:42'),
(6, 'pres_share', '0', '2015-05-18 19:58:02', '2015-05-18 19:58:02'),
(7, 'vpres_share', '0', '2015-05-18 19:58:27', '2015-05-18 19:58:27'),
(8, 'company_share', '0', '2015-05-18 19:58:54', '2015-05-18 19:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referrals`
--

CREATE TABLE IF NOT EXISTS `direct_referrals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recruiter_id` int(11) DEFAULT NULL,
  `recruitee_id` int(11) DEFAULT NULL,
  `dr_earnings` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `direct_referrals`
--

INSERT INTO `direct_referrals` (`id`, `recruiter_id`, `recruitee_id`, `dr_earnings`, `created_at`, `updated_at`) VALUES
(1, 6, 7, 100.00, '2017-06-13 19:19:52', '2017-06-13 19:19:52'),
(2, 7, 8, 100.00, '2017-06-13 19:22:22', '2017-06-13 19:22:22'),
(3, 7, 9, 100.00, '2017-06-13 19:24:11', '2017-06-13 19:24:11'),
(4, 7, 10, 100.00, '2017-06-13 19:26:11', '2017-06-13 19:26:11'),
(5, 7, 11, 100.00, '2017-06-13 19:27:55', '2017-06-13 19:27:55'),
(6, 7, 12, 100.00, '2017-06-13 19:32:42', '2017-06-13 19:32:42'),
(7, 7, 13, 100.00, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `exit_history`
--

CREATE TABLE IF NOT EXISTS `exit_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT '10000.00',
  `table` int(8) DEFAULT '1',
  `level` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exit_history`
--

INSERT INTO `exit_history` (`id`, `user_id`, `value`, `table`, `level`, `created_at`, `updated_at`) VALUES
(1, 7, 1000.00, 1, 0, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE IF NOT EXISTS `general_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `module_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'wallet_address', '{"value":"2N7t14vJE6KxGooZ2EeERpokzWzLRi8hyBu"}', '2016-09-09 03:54:22', NULL),
(2, 'wallet_username', '{"value":"company_wallet"}', '2016-09-09 03:54:40', NULL),
(3, 'wallet_password', '{"value":"UpNext-Trading"}', '2016-09-09 03:54:53', NULL),
(4, 'codes_amount', '{"value":"1500"}', '2016-09-09 03:54:53', NULL),
(5, 'dr_bonus', '{"value":"100"}', '2016-09-09 03:54:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{"admin":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(2, 'Members', '{"member":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `hierarchy_siblings`
--

CREATE TABLE IF NOT EXISTS `hierarchy_siblings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recruitee_id` int(11) NOT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_has_users_users4_idx` (`recruitee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `hierarchy_siblings`
--

INSERT INTO `hierarchy_siblings` (`id`, `recruitee_id`, `position`, `created_at`, `updated_at`) VALUES
(1, 7, 0, '2017-06-13 19:19:52', '2017-06-13 19:19:52'),
(2, 8, 0, '2017-06-13 19:22:22', '2017-06-13 19:22:22'),
(3, 9, 1, '2017-06-13 19:24:11', '2017-06-13 19:24:11'),
(4, 10, 0, '2017-06-13 19:26:11', '2017-06-13 19:26:11'),
(5, 11, 1, '2017-06-13 19:27:55', '2017-06-13 19:27:55'),
(6, 12, 0, '2017-06-13 19:32:42', '2017-06-13 19:32:42'),
(7, 13, 1, '2017-06-13 19:37:55', '2017-06-13 19:37:55'),
(8, 7, 0, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE IF NOT EXISTS `master_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'eb0a191797624dd3a48fa681d3061212', 1, '2015-05-24 11:19:47', '2015-06-17 01:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `member_payout_requests`
--

CREATE TABLE IF NOT EXISTS `member_payout_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payout_method` text,
  `table` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(500) DEFAULT NULL,
  `product_name` text,
  `product_description` text,
  `product_details` text,
  `product_ratings` decimal(10,2) DEFAULT '0.00',
  `product_feedback` text,
  `product_remarks` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products_by_categories`
--

CREATE TABLE IF NOT EXISTS `products_by_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `category_name` text,
  `category_description` text,
  `category_details` text NOT NULL,
  `icon_path` varchar(250) DEFAULT NULL,
  `category_ratings` decimal(10,2) DEFAULT '0.00',
  `category_feedback` text,
  `category_remarks` text,
  `is_parent` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_activation`
--

CREATE TABLE IF NOT EXISTS `purchase_activation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `payment_method` tinyint(1) DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  `code_amount` decimal(10,2) DEFAULT '0.00',
  `approved_by` int(11) DEFAULT NULL,
  `company_wallet_address` varchar(250) DEFAULT NULL,
  `sender_wallet_address` varchar(250) DEFAULT NULL,
  `btc_received_amount` decimal(10,8) DEFAULT '0.00000000',
  `remarks` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `purchase_activation`
--

INSERT INTO `purchase_activation` (`id`, `reference_code`, `user_id`, `total_amount`, `payment_method`, `quantity`, `code_amount`, `approved_by`, `company_wallet_address`, `sender_wallet_address`, `btc_received_amount`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'nhu4fW6QzN', 7, 1500.00, 1, 1, 1500.00, 1, NULL, NULL, 0.00000000, '', 1, '2017-06-13 19:14:36', '2017-06-13 19:14:54'),
(2, 'myiSk4jZ0z', 7, 10500.00, 1, 7, 1500.00, 1, NULL, NULL, 0.00000000, '', 1, '2017-06-13 19:20:48', '2017-06-13 19:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_activation_proof`
--

CREATE TABLE IF NOT EXISTS `purchase_activation_proof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_activation_id` int(11) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_throttle_users_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_code` varchar(255) DEFAULT 'XXXX-XXXX-XXXX',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `canonical_hash` varchar(500) DEFAULT NULL,
  `pin_code` varchar(50) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT '',
  `avatar` varchar(500) DEFAULT 'default-avatar.jpg',
  `birthday` date DEFAULT NULL,
  `address` text,
  `contact_no` varchar(255) DEFAULT NULL,
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '0' COMMENT '1=Admin, 2=Member',
  `package_type` tinyint(1) DEFAULT '0',
  `matrix_count` int(11) NOT NULL DEFAULT '2',
  `activated` tinyint(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `company_associated_wallet` varchar(250) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '1',
  `is_for_activation` tinyint(1) DEFAULT '0',
  `is_leader` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=14 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_code`, `username`, `email`, `password`, `canonical_hash`, `pin_code`, `full_name`, `avatar`, `birthday`, `address`, `contact_no`, `permissions`, `user_type`, `package_type`, `matrix_count`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `company_associated_wallet`, `is_primary`, `is_for_activation`, `is_leader`, `created_at`, `updated_at`) VALUES
(1, 'u61J14805250195Agq', 'admin1', 'admin1@upnext.shop', '$2y$10$JwPNRtXKzz7xwomyeNeH1uiIjTIjlpyPNa57xMUGSezshmOoCKPqq', 'YWRtaW4x', '1234', 'Administrator 1', 'default-avatar.jpg', NULL, NULL, NULL, '{"admin":1}', 1, 0, 2, 1, '', NULL, '2017-06-13 19:14:46', '$2y$10$Uwy0EyPK4ap20.ab8x2OHu2WmQfJuAGWLp/5YiSjuXO2TqECLQpoa', NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2017-06-13 19:14:46'),
(2, 'C14U14805250194l4c', 'admin2', 'admin2@upnext.shop', '$2y$10$fXbQAu.h4q0hXbp/8curlO994aYAWfWee06OCx8AbqG.eGOo97Zv6', 'YWRtaW4y', '1234', 'Administrator 2', 'default-avatar.jpg', NULL, NULL, NULL, '{"admin":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(3, 'eLFx1480525019skte', 'admin3', 'admin3@upnext.shop', '$2y$10$sqMt9lOvW/3YWcBOzzyaOuj/fXnZDM4LV8FQIJQ5BtJFlrid/B/hq', 'YWRtaW4z', '1234', 'Administrator 3', 'default-avatar.jpg', NULL, NULL, NULL, '{"admin":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(4, 'b3Fq1480525019tIyn', 'admin4', 'admin4@upnext.shop', '$2y$10$MMtqDhjtieoTRo2lZLJ68Ok.IvqhmvVte3szKKRos.KvJ1pVcta56', 'YWRtaW40', '1234', 'Administrator 4', 'default-avatar.jpg', NULL, NULL, NULL, '{"admin":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(5, 'YW6b1480525019KyKX', 'admin5', 'admin5@upnext.shop', '$2y$10$UAnctLyJtnViiSECjuDMzuGTEHyflFEqJKsWGcpWh6MQ8A2k7wO9i', 'YWRtaW41', '1234', 'Administrator 5', 'default-avatar.jpg', NULL, NULL, NULL, '{"admin":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(6, 'JEA11480525020RYeB', 'upnext', 'support@upnext.shop', '$2y$10$DDNNMnMfX2sE4lPRICy2Vem0oHrbRHwworJ3GCS8aKSautqj1pQBi', 'cGFzc3dvcmQxMjM0', '1234', 'Company Head', 'default-avatar.jpg', NULL, NULL, NULL, '{"member":1}', 2, 0, 2, 1, '', NULL, '2017-06-12 14:03:10', '$2y$10$uQCoVLKT1NJoCV7JT/VodeP///f2i4FrxfYzmMVtwlpjSa29onZly', NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:57:00', '2017-06-12 14:03:10'),
(7, 'PRbq1497352432LKPD', 'UN8VT5P', 'upnextwebmaster@gmail.com', '$2y$10$9r7f8pG09GpCmWwxQw7Op.hD29kxGQtY4Z1/DeG4oz6CtBBTx7Sbm', 'RzVEbFBERjB0YmFq', 'EYSt', 'UPNEXT01', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:14:27', '2017-06-13 19:38:59', '$2y$10$NFMsn05GXkyUr6kZjxc9fuO8b29iOog93cVORlUe6y9GHS1Vsi39e', NULL, 1, NULL, 1, 1, 1, '2017-06-13 19:13:52', '2017-06-13 19:38:59'),
(8, 'PEHX1497352942XmJn', 'UNB2NTT', 'upnextwebmaster@gmail.com', '$2y$10$U.V/wsnSpYJbKGYncaJg9ObZ6j4C4Lsz5KgcZIdm1JJn9zuSVHAxm', 'N0MzWktFaHlsTm1x', 'CBSm', 'UPNEXT02', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:22:49', '2017-06-13 19:22:49', '$2y$10$WLCx4FYsx90vizGffKdeOeZ8lPmUfGrGxxJ0ySuLs1XSL1/9MUsK.', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:22:22', '2017-06-13 19:22:49'),
(9, 'TbC41497353050kVCJ', 'UN7XBXX', 'upnextwebmaster@gmail.com', '$2y$10$5Eh6Zg3WX3FDatv3.4jK8uWRE2g8XcgtFluTn/c2vUfv/a1A5cRQ2', 'QmtMeEs4MDlkdjA0', 'Z701', 'UPNEXT03', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:24:35', '2017-06-13 19:24:35', '$2y$10$h258datyOzZXhgyh5C5aXOI2oNe3gG2h5b5NLVaF6TLyQHrFwPXe6', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:24:11', '2017-06-13 19:24:35'),
(10, '4Q3m14973531715BUY', 'UNI0XIM', 'upnextwebmaster@gmail.com', '$2y$10$J2FPVCfsGYQYiMa3htUsp.aExsAZJwf0nR4PI3vs6BycOG00TgEjK', 'V2I1YlBza3JlNVQ3', 'ZnMn', 'UPNEXT04', 'default-avatar.jpg', '2017-06-19', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:26:35', '2017-06-13 19:26:35', '$2y$10$1QCusRIlKiO36pYRQL3cNezfGkkfpTbyaptO9wJh5vnchAIWRdCRu', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:26:11', '2017-06-13 19:26:35'),
(11, 'ukmH1497353274XIBd', 'UNM4M4Y', 'upnextwebmaster@gmail.com', '$2y$10$olFksKyxwxZS/uSPDyPkPORx0pGkNvLgTIPZk0XzmQYi1gpM6PLtm', 'S0FaYTN2aVd2QjFa', '7Hjj', 'UPNEXT05', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:29:51', '2017-06-13 19:29:52', '$2y$10$1bj/tyTjC.mngyYN81Gl7enssBr76JB1NHEVYgE5YLDaSVEq/UUV2', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:27:55', '2017-06-13 19:29:52'),
(12, 'j4tE1497353562ArqN', 'UN2WKB4', 'upnextwebmaster@gmail.com', '$2y$10$K9KdFuGeB5.EKyRC5TmUOuSyOKTxTBf2wOmJrGh5FC8n7/VkNMPOy', 'MElqbkYzVGM3bkl5', 'pX96', 'UPNEXT07', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:33:18', '2017-06-13 19:33:18', '$2y$10$KX9KIUefKTcwxmYQCstBCeVTELNjn0M9OZ2W1.d0Crn2aEMdgO72y', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:32:42', '2017-06-13 19:33:18'),
(13, 'Q9911497353875gfjV', 'UNNXUCU', 'upnextwebmaster@gmail.com', '$2y$10$7BQiGEbRM3D3Ll0VxzHwWu4HI9m5p45wCXCQz84Lm9TZkoGd6RNie', 'TVpoMFBYWTl0WERV', 'mBFs', 'UPNEXT08', 'default-avatar.jpg', '2017-06-12', '746 G. De Borja St., Pateros', '092-77-473199', '{"member":1}', 2, 2, 5, 1, NULL, '2017-06-13 19:38:42', '2017-06-13 19:38:42', '$2y$10$takMsZb6Tp8dWsBRxRvGI.k.t5I.nst0Pk2aRkmaRpjJhRSZz0yF2', NULL, 1, NULL, 1, 2, 0, '2017-06-13 19:37:55', '2017-06-13 19:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `fk_users_group_groups1_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_hierarchy_siblings`
--

CREATE TABLE IF NOT EXISTS `users_has_hierarchy_siblings` (
  `user_id` int(11) NOT NULL,
  `hierarchy_sibling_id` int(11) NOT NULL,
  `table` int(8) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`,`hierarchy_sibling_id`),
  KEY `fk_users_has_hierarchy_children_hierarchy_children1_idx` (`hierarchy_sibling_id`),
  KEY `fk_users_has_hierarchy_children_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_has_hierarchy_siblings`
--

INSERT INTO `users_has_hierarchy_siblings` (`user_id`, `hierarchy_sibling_id`, `table`) VALUES
(6, 1, 1),
(6, 8, 2),
(7, 2, 1),
(7, 3, 1),
(8, 4, 1),
(8, 5, 1),
(9, 6, 1),
(9, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_balance`
--

CREATE TABLE IF NOT EXISTS `user_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `current_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_balance` decimal(10,2) DEFAULT '0.00',
  `table` int(1) DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_balance`
--

INSERT INTO `user_balance` (`id`, `user_id`, `current_balance`, `total_balance`, `table`, `updated_at`, `created_at`) VALUES
(1, 7, 1000.00, 1000.00, 1, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `voucher_amount` decimal(10,2) DEFAULT '0.00',
  `image_path` varchar(500) DEFAULT NULL,
  `is_voucher` tinyint(1) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `ref_code`, `user_id`, `voucher_amount`, `image_path`, `is_voucher`, `is_used`, `created_at`, `updated_at`) VALUES
(1, 'UPNXT1-nhu4fW6QzN', 7, 750.00, 'assets/images/stub/PRbq1497352432LKPD/UN8VT5P_claim_stub.jpg', 0, 0, '2017-06-13 19:14:54', '2017-06-13 19:14:55'),
(2, 'UPNXT2-sQgvvFR0', 8, 750.00, 'assets/images/stub/PEHX1497352942XmJn/UNB2NTT_claim_stub.jpg', 0, 0, '2017-06-13 19:22:22', '2017-06-13 19:22:22'),
(3, 'UPNXT2-LgpBbnlG', 9, 750.00, 'assets/images/stub/TbC41497353050kVCJ/UN7XBXX_claim_stub.jpg', 0, 0, '2017-06-13 19:24:11', '2017-06-13 19:24:11'),
(4, 'UPNXT2-PYSYDW6g', 10, 750.00, 'assets/images/stub/4Q3m14973531715BUY/UNI0XIM_claim_stub.jpg', 0, 0, '2017-06-13 19:26:11', '2017-06-13 19:26:11'),
(5, 'UPNXT2-T1NkKipy', 11, 750.00, 'assets/images/stub/ukmH1497353274XIBd/UNM4M4Y_claim_stub.jpg', 0, 0, '2017-06-13 19:27:55', '2017-06-13 19:27:55'),
(6, 'UPNXT2-YWQpFBnH', 12, 750.00, 'assets/images/stub/j4tE1497353562ArqN/UN2WKB4_claim_stub.jpg', 0, 0, '2017-06-13 19:32:43', '2017-06-13 19:32:43'),
(7, 'UPNXT3-msZ2YEdt', 7, 1500.00, 'assets/images/vouchers/PRbq1497352432LKPD/UN8VT5P_voucher.jpg', 1, 0, '2017-06-13 19:37:55', '2017-06-13 19:37:55'),
(8, 'UPNXT2-Znldkpjr', 13, 750.00, 'assets/images/stub/Q9911497353875gfjV/UNNXUCU_claim_stub.jpg', 0, 0, '2017-06-13 19:37:55', '2017-06-13 19:37:55');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_users_users4` FOREIGN KEY (`recruitee_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `fk_throttle_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_group_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_group_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_hierarchy_children_hierarchy_children1` FOREIGN KEY (`hierarchy_sibling_id`) REFERENCES `hierarchy_siblings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_hierarchy_children_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
