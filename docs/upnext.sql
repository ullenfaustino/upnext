-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 13, 2017 at 04:51 PM
-- Server version: 5.7.18
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upnext`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_control`
--

CREATE TABLE `admin_control` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_control`
--

INSERT INTO `admin_control` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'member_registration', '0', '2015-08-17 06:06:59', '2015-08-17 21:06:59'),
(2, 'member_payout', '0', '2015-08-25 07:32:19', '2015-08-25 22:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `bank_name` text,
  `bank_account_no` varchar(500) DEFAULT NULL,
  `bank_account_name` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `bank_account_no`, `bank_account_name`, `created_at`, `updated_at`) VALUES
(1, 'Bank of the Philippines Island', '123456789876', 'Lorem Ipsum Dolor Soene', '2017-06-11 06:35:50', NULL),
(2, 'Security Bank', '987654321', 'Lorem Ipsum Dolor Soene', '2017-06-11 06:35:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE `bulletin` (
  `id` int(8) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` int(8) DEFAULT '80',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `generated_code` varchar(45) DEFAULT NULL,
  `purchase_activation_id` int(11) DEFAULT NULL,
  `user_id` int(8) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_allocation`
--

CREATE TABLE `company_allocation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_cost` decimal(12,2) DEFAULT '160.00',
  `operating_exp` decimal(12,2) DEFAULT '50.00',
  `incentives` decimal(12,2) DEFAULT '20.00',
  `misc` decimal(12,2) DEFAULT '20.00',
  `direct` decimal(12,2) DEFAULT '100.00',
  `matrix` decimal(12,2) DEFAULT '980.00',
  `company_a_profit` decimal(12,2) DEFAULT '85.00',
  `company_b_profit` decimal(12,2) DEFAULT '85.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `company_pullouts`
--

CREATE TABLE `company_pullouts` (
  `id` int(11) NOT NULL,
  `alloc_name` varchar(500) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT '0.00',
  `remarks` text,
  `admin_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '""',
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'entry_amount', '12000', '2015-05-12 15:00:06', '2015-05-12 15:00:06'),
(2, 'direct_referral_bonus', '0', '2015-05-17 14:51:19', '2015-05-17 14:51:19'),
(3, 'exit_bonus', '36000', '2015-05-18 19:56:52', '2015-05-18 19:56:52'),
(4, 'misc', '0', '2015-05-18 19:57:14', '2015-05-18 19:57:14'),
(5, 'conceptualization_fee', '0', '2015-05-18 19:57:42', '2015-05-18 19:57:42'),
(6, 'pres_share', '0', '2015-05-18 19:58:02', '2015-05-18 19:58:02'),
(7, 'vpres_share', '0', '2015-05-18 19:58:27', '2015-05-18 19:58:27'),
(8, 'company_share', '0', '2015-05-18 19:58:54', '2015-05-18 19:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referrals`
--

CREATE TABLE `direct_referrals` (
  `id` int(11) NOT NULL,
  `recruiter_id` int(11) DEFAULT NULL,
  `recruitee_id` int(11) DEFAULT NULL,
  `dr_earnings` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `exit_history`
--

CREATE TABLE `exit_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT '10000.00',
  `table` int(8) DEFAULT '1',
  `level` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `module_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'wallet_address', '{\"value\":\"2N7t14vJE6KxGooZ2EeERpokzWzLRi8hyBu\"}', '2016-09-09 03:54:22', NULL),
(2, 'wallet_username', '{\"value\":\"company_wallet\"}', '2016-09-09 03:54:40', NULL),
(3, 'wallet_password', '{\"value\":\"UpNext-Trading\"}', '2016-09-09 03:54:53', NULL),
(4, 'codes_amount', '{\"value\":\"1500\"}', '2016-09-09 03:54:53', NULL),
(5, 'dr_bonus', '{\"value\":\"100\"}', '2016-09-09 03:54:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{\"admin\":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(2, 'Members', '{\"member\":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `hierarchy_siblings`
--

CREATE TABLE `hierarchy_siblings` (
  `id` int(11) NOT NULL,
  `recruitee_id` int(11) NOT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE `master_password` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'eb0a191797624dd3a48fa681d3061212', 1, '2015-05-24 11:19:47', '2015-06-17 01:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `member_payout_requests`
--

CREATE TABLE `member_payout_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payout_method` text,
  `table` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_code` varchar(500) DEFAULT NULL,
  `product_name` text,
  `product_description` text,
  `product_details` text,
  `product_ratings` decimal(10,2) DEFAULT '0.00',
  `product_feedback` text,
  `product_remarks` text,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products_by_categories`
--

CREATE TABLE `products_by_categories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `category_name` text,
  `category_description` text,
  `category_details` text NOT NULL,
  `icon_path` varchar(250) DEFAULT NULL,
  `category_ratings` decimal(10,2) DEFAULT '0.00',
  `category_feedback` text,
  `category_remarks` text,
  `is_parent` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_activation`
--

CREATE TABLE `purchase_activation` (
  `id` int(11) NOT NULL,
  `reference_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `payment_method` tinyint(1) DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  `code_amount` decimal(10,2) DEFAULT '0.00',
  `approved_by` int(11) DEFAULT NULL,
  `company_wallet_address` varchar(250) DEFAULT NULL,
  `sender_wallet_address` varchar(250) DEFAULT NULL,
  `btc_received_amount` decimal(10,8) DEFAULT '0.00000000',
  `remarks` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_activation_proof`
--

CREATE TABLE `purchase_activation_proof` (
  `id` int(11) NOT NULL,
  `purchase_activation_id` int(11) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(255) DEFAULT 'XXXX-XXXX-XXXX',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `canonical_hash` varchar(500) DEFAULT NULL,
  `pin_code` varchar(50) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT '',
  `avatar` varchar(500) DEFAULT 'default-avatar.jpg',
  `birthday` date DEFAULT NULL,
  `address` text,
  `contact_no` varchar(255) DEFAULT NULL,
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '0' COMMENT '1=Admin, 2=Member',
  `package_type` tinyint(1) DEFAULT '0',
  `matrix_count` int(11) NOT NULL DEFAULT '2',
  `activated` tinyint(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `company_associated_wallet` varchar(250) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '1',
  `is_for_activation` tinyint(1) DEFAULT '0',
  `is_leader` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_code`, `username`, `email`, `password`, `canonical_hash`, `pin_code`, `full_name`, `avatar`, `birthday`, `address`, `contact_no`, `permissions`, `user_type`, `package_type`, `matrix_count`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `company_associated_wallet`, `is_primary`, `is_for_activation`, `is_leader`, `created_at`, `updated_at`) VALUES
(1, 'u61J14805250195Agq', 'admin1', 'admin1@upnext.shop', '$2y$10$JwPNRtXKzz7xwomyeNeH1uiIjTIjlpyPNa57xMUGSezshmOoCKPqq', 'YWRtaW4x', '1234', 'Administrator 1', 'default-avatar.jpg', NULL, NULL, NULL, '{\"admin\":1}', 1, 0, 2, 1, '', NULL, '2017-06-13 07:29:00', '$2y$10$N6ahU6n3zmuIhBiZZ42kneAdPptfbPZtUF2AvxQ/W6G9C2RWa3n5u', NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2017-06-13 07:29:00'),
(2, 'C14U14805250194l4c', 'admin2', 'admin2@upnext.shop', '$2y$10$fXbQAu.h4q0hXbp/8curlO994aYAWfWee06OCx8AbqG.eGOo97Zv6', 'YWRtaW4y', '1234', 'Administrator 2', 'default-avatar.jpg', NULL, NULL, NULL, '{\"admin\":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(3, 'eLFx1480525019skte', 'admin3', 'admin3@upnext.shop', '$2y$10$sqMt9lOvW/3YWcBOzzyaOuj/fXnZDM4LV8FQIJQ5BtJFlrid/B/hq', 'YWRtaW4z', '1234', 'Administrator 3', 'default-avatar.jpg', NULL, NULL, NULL, '{\"admin\":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(4, 'b3Fq1480525019tIyn', 'admin4', 'admin4@upnext.shop', '$2y$10$MMtqDhjtieoTRo2lZLJ68Ok.IvqhmvVte3szKKRos.KvJ1pVcta56', 'YWRtaW40', '1234', 'Administrator 4', 'default-avatar.jpg', NULL, NULL, NULL, '{\"admin\":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(5, 'YW6b1480525019KyKX', 'admin5', 'admin5@upnext.shop', '$2y$10$UAnctLyJtnViiSECjuDMzuGTEHyflFEqJKsWGcpWh6MQ8A2k7wO9i', 'YWRtaW41', '1234', 'Administrator 5', 'default-avatar.jpg', NULL, NULL, NULL, '{\"admin\":1}', 1, 0, 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(6, 'JEA11480525020RYeB', 'upnext', 'support@upnext.shop', '$2y$10$DDNNMnMfX2sE4lPRICy2Vem0oHrbRHwworJ3GCS8aKSautqj1pQBi', 'cGFzc3dvcmQxMjM0', '1234', 'Company Head', 'default-avatar.jpg', NULL, NULL, NULL, '{\"member\":1}', 2, 0, 2, 1, '', NULL, '2017-06-12 14:03:10', '$2y$10$uQCoVLKT1NJoCV7JT/VodeP///f2i4FrxfYzmMVtwlpjSa29onZly', NULL, 1, NULL, 1, 0, 0, '2016-11-30 16:57:00', '2017-06-12 14:03:10');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_hierarchy_siblings`
--

CREATE TABLE `users_has_hierarchy_siblings` (
  `user_id` int(11) NOT NULL,
  `hierarchy_sibling_id` int(11) NOT NULL,
  `table` int(8) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_balance`
--

CREATE TABLE `user_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `current_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_balance` decimal(10,2) DEFAULT '0.00',
  `table` int(1) DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `voucher_amount` decimal(10,2) DEFAULT '0.00',
  `image_path` varchar(500) DEFAULT NULL,
  `is_voucher` tinyint(1) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_control`
--
ALTER TABLE `admin_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_allocation`
--
ALTER TABLE `company_allocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_pullouts`
--
ALTER TABLE `company_pullouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exit_history`
--
ALTER TABLE `exit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_has_users_users4_idx` (`recruitee_id`);

--
-- Indexes for table `master_password`
--
ALTER TABLE `master_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_activation`
--
ALTER TABLE `purchase_activation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_activation_proof`
--
ALTER TABLE `purchase_activation_proof`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_group_groups1_idx` (`group_id`);

--
-- Indexes for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD PRIMARY KEY (`user_id`,`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_hierarchy_children1_idx` (`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_users1_idx` (`user_id`);

--
-- Indexes for table `user_balance`
--
ALTER TABLE `user_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_control`
--
ALTER TABLE `admin_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bulletin`
--
ALTER TABLE `bulletin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_allocation`
--
ALTER TABLE `company_allocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_pullouts`
--
ALTER TABLE `company_pullouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `constants`
--
ALTER TABLE `constants`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exit_history`
--
ALTER TABLE `exit_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_password`
--
ALTER TABLE `master_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_activation`
--
ALTER TABLE `purchase_activation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_activation_proof`
--
ALTER TABLE `purchase_activation_proof`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_balance`
--
ALTER TABLE `user_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_users_users4` FOREIGN KEY (`recruitee_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `fk_throttle_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_group_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_group_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_hierarchy_children_hierarchy_children1` FOREIGN KEY (`hierarchy_sibling_id`) REFERENCES `hierarchy_siblings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_hierarchy_children_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
