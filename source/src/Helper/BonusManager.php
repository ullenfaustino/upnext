<?php
namespace Helper;

use \UsersHasHierarchySiblings;
use \HierarchySiblings;
use \UserBalance;
use \ExitHistory;
use \Sentry;
use \DirectReferrals;
use \Users;
use \Vouchers;
use \CompanyAllocation;

class BonusManager {

	public static function processExitBonus($table, $BonusAmount) {
		$manager_cmd = sprintf("php %s/exit_manager.php %s %s &", WORKERS_PATH, $table, $BonusAmount);
		pclose(popen($manager_cmd, "w"));
	}

	public static function updateUserBalance($userId, $balToAdd, $table = 1) {
		$balance = UserBalance::where('user_id', '=', $userId) -> where('table', '=', $table) -> first();

		if ($balance == null) {
			$balance = new UserBalance();
			$balance -> user_id = $userId;
			$balance -> table = $table;
		}

		$balance -> current_balance += $balToAdd;
		$balance -> total_balance += $balToAdd;
		$balance -> save();
		
		$codeGen = new CodeGenerator();
		$user = Users::find($userId);
		
		$voucher = new Vouchers();
		$voucher -> ref_code = "UPNXT3-" . $codeGen -> getToken(8);
		$voucher -> user_id = $user -> id;
		$voucher -> voucher_amount = 1500;
		$voucher -> is_voucher = 1;
		$voucher -> save();
		
		$params["reference_code"] = $user -> ref_code;
		$params["transaction_code"] = $voucher -> ref_code;
		$params["account_code"] = $user -> username;
		$params["account_name"] = $user -> full_name;
		$params["amount"] = $voucher -> voucher_amount;
		$params["date_issued"] = date("F j, Y", strtotime ( $voucher->created_at ));
		$params["email"] = $user -> email;
		$mode = "voucher";
		$template_cmd = sprintf("php %s/TemplateGenerator.php %s %s &", WORKERS_PATH, $mode, base64_encode(json_encode($params)));
		pclose(popen($template_cmd, "w"));
	}
	
	public static function setCompanyAllocation($user_id) {
		$alloc = new CompanyAllocation();
		$alloc -> user_id = $user_id;
		$alloc -> product_cost = 160;
		$alloc -> operating_exp = 50;
		$alloc -> incentives = 20;
		$alloc -> misc = 20;
		$alloc -> direct = 100;
		$alloc -> matrix = 980;
		$alloc -> company_a_profit = 85;
		$alloc -> company_b_profit = 85;
		$alloc -> save();
	}

	public static function getPosition($table) {
		$upnext = Sentry::findUserByLogin('upnext');
		$hierarchies = UsersHasHierarchySiblings::where('table', '=', $table) 
		                                  -> groupBy('user_id') 
		                                  // -> orderBy('created_at', 'asc') 
		                                  -> get();
		if (count($hierarchies) > 0) {
			$hasSibling = false;
			$da = false;
			foreach ($hierarchies as $key => $h) {
				$siblings = UsersHasHierarchySiblings::siblings($h -> user_id, $table) -> get();
				if (count($siblings) < 2 && $h -> user_id == $upnext -> id) {
					$USH = UsersHasHierarchySiblings::leftJoin("hierarchy_siblings as HS", "HS.id", "=", "users_has_hierarchy_siblings.hierarchy_sibling_id") -> where("users_has_hierarchy_siblings.user_id", "=", $h -> user_id) -> where("users_has_hierarchy_siblings.table", "=", $table) -> select(array("recruitee_id")) -> first();
					$sib = UsersHasHierarchySiblings::siblings($USH -> recruitee_id, $table) -> get();
					if (count($sib) == 0) {
						$head = $USH -> recruitee_id;
						$position = "Left";
						$da = true;
						break;
					} elseif (count($sib) == 1) {
						$head = $USH -> recruitee_id;
						$position = "Right";
						$da = true;
						break;
					} elseif (count($sib) > 1) {
						continue;
					}
				}

				$pos = BonusManager::getLinePosition($upnext -> id, $table);
				$head = $pos["Head"];
				$position = $pos["Position"];
			}
		} else {
			$main = Sentry::findUserByLogin('upnext');
			$head = $main -> id;
			$position = "Left";
		}
		return array('Position' => $position, 'Head' => $head);
	}

	public static function getLinePosition($userId, $table) {
		$hierarchies = BonusManager::getheirarchies($userId, $table);
		$hierarchies = explode(" ", trim($hierarchies));
		$arrH = array();
		foreach ($hierarchies as $key => $hierarchy) {
			if ($hierarchy != "") {
				array_push($arrH, $hierarchy);
			}
		}
		sort($arrH);
		// var_dump($arrH);
		// exit();
		if (count($arrH) == 0) {
			return array('Position' => "Left", 'Head' => $userId);
		}
		foreach ($arrH as $key => $id) {
			$siblings = UsersHasHierarchySiblings::siblings($id, $table) -> get();
			$hasLeft = false;
			$hasRight = false;
			$left_count = 0;
			$right_count = 0;
			$left_recruitee_id = 0;
			$right_recruitee_id = 0;
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$hasLeft = true;
					$left_recruitee_id = $sibling -> recruitee_id;
					$left_count = UsersHasHierarchySiblings::siblings($sibling -> recruitee_id, $table) -> count();
				} else if ($sibling -> position == 1) {
					$hasRight = true;
					$right_recruitee_id = $sibling -> recruitee_id;
					$right_count = UsersHasHierarchySiblings::siblings($sibling -> recruitee_id, $table) -> count();
				}
			}
			if (!$hasLeft && $hasRight) {
				$head = $id;
				$position = "Left";
				break;
			} else if ($hasLeft && !$hasRight) {
				$head = $id;
				$position = "Right";
				break;
			} else {
				if ($left_count == 0) {
					$head = $left_recruitee_id;
					$position = "Left";
					break;
				} else if ($left_count == 1) {
					$rights = UsersHasHierarchySiblings::siblings($left_recruitee_id, $table) -> get();
					foreach ($rights as $key => $sib) {
						if ($sib -> position == 0) {
							$head = $left_recruitee_id;
							$position = "Right";
						} elseif ($sib -> position == 1) {
							$head = $left_recruitee_id;
							$position = "Left";
						}
					}
					break;
				} else if ($right_count == 0) {
					$head = $right_recruitee_id;
					$position = "Left";
					break;
				} else if ($right_count == 1) {
					$rights = UsersHasHierarchySiblings::siblings($right_recruitee_id, $table) -> get();
					foreach ($rights as $key => $sib) {
						if ($sib -> position == 0) {
							$head = $right_recruitee_id;
							$position = "Right";
						} elseif ($sib -> position == 1) {
							$head = $right_recruitee_id;
							$position = "Left";
						}
					}
					break;
				} else {
					continue;
				}
			}
		}
		return array('Position' => $position, 'Head' => $head);
	}

	private static function getheirarchies($userId, $table) {
		$arrAnak = array();
		$siblings = UsersHasHierarchySiblings::siblings($userId, $table) -> get();
		foreach ($siblings as $key => $h) {
			array_push($arrAnak, $h -> recruitee_id);
			array_push($arrAnak, BonusManager::getheirarchies($h -> recruitee_id, $table));
		}
		return implode(" ", $arrAnak);
	}

	public static function getLineCount($userId, $table) {
		$count = 0;

		$siblings = UsersHasHierarchySiblings::siblings($userId, $table) -> get();
		foreach ($siblings as $key => $sibling) {
			$count++;
			$count += BonusManager::getLineCount($sibling -> recruitee_id, $table);
		}

		return $count;
	}

	public function getHierarchyCount($userId, $table) {
		$leftCount = 0;
		$rightCount = 0;

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $userId) -> first();
		if ($h) {
			$siblings = UsersHasHierarchySiblings::siblings($h -> user_id, $table) -> get();
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$leftCount++;
					$leftCount += BonusManager::getLineCount($sibling -> recruitee_id, $table);
				} else if ($sibling -> position == 1) {
					$rightCount++;
					$rightCount += BonusManager::getLineCount($sibling -> recruitee_id, $table);
				}
			}
		}
		return array('LEFT' => $leftCount, 'RIGHT' => $rightCount);
	}

	public function processUnilevel($user_id, $amount) {
		try {
			$unilevel = BonusManager::unilevels($user_id);

			$allocated_amount = $btc_amount * 0.10;
			foreach ($unilevel as $key => $u) {
				$credit = $u['percentage'] * $allocated_amount;

				if (!is_null($u['user_id'])) {
					$member = Users::find($u['user_id']);

					// $member -> unilevel_balance += GenericHelper::BitcoinToARc($credit);
					// $member -> save();
					//
					// $logs = new UnilevelLogs();
					// $logs -> ref_id = $member -> id;
					// $logs -> coin_amount = GenericHelper::BitcoinToARc($credit);
					// $logs -> level = $key + 1;
					// $logs -> from_ref_id = $user_id;
					// $logs -> interest = $u['percentage'];
					// $logs -> save();
				} else {
					// GenericHelper::processFlushOutlogs($user_id, GenericHelper::BitcoinToARc($credit), "Unilevel", $key + 1);
				}
			}
		} catch(\Exception $e) {
			// echo $e -> getMessage();
			throw new \Exception($e -> getMessage());
		}
	}

	private function unilevels($member_id) {
		$ulvl[0]['percentage'] = 0.03;
		$ulvl[0]['user_id'] = null;

		$ulvl[1]['percentage'] = 0.03;
		$ulvl[1]['user_id'] = null;

		$ulvl[2]['percentage'] = 0.06;
		$ulvl[2]['user_id'] = null;

		$ulvl[3]['percentage'] = 0.03;
		$ulvl[3]['user_id'] = null;

		$ulvl[4]['percentage'] = 0.03;
		$ulvl[4]['user_id'] = null;

		$ulvl[5]['percentage'] = 0.06;
		$ulvl[5]['user_id'] = null;

		$ulvl[6]['percentage'] = 0.03;
		$ulvl[6]['user_id'] = null;

		$ulvl[7]['percentage'] = 0.03;
		$ulvl[7]['user_id'] = null;

		$ulvl[8]['percentage'] = 0.06;
		$ulvl[8]['user_id'] = null;

		$ulvl[9]['percentage'] = 0.09;
		$ulvl[9]['user_id'] = null;

		// 1st level
		$first_level = DirectReferrals::where('recruitee_id', '=', $member_id) -> first();
		if ($first_level) {
			$ulvl[0]['user_id'] = $first_level -> recruiter_id;

			// 2nd level
			$second_level = DirectReferrals::where('recruitee_id', '=', $first_level -> recruiter_id) -> first();
			if ($second_level) {
				$ulvl[1]['user_id'] = $second_level -> recruiter_id;

				// 3rd level
				$third_level = DirectReferrals::where('recruitee_id', '=', $second_level -> recruiter_id) -> first();
				if ($third_level) {
					$ulvl[2]['user_id'] = $third_level -> recruiter_id;

					// 4th level
					$fourth_level = DirectReferrals::where('recruitee_id', '=', $third_level -> recruiter_id) -> first();
					if ($fourth_level) {
						$ulvl[3]['user_id'] = $fourth_level -> recruiter_id;

						// 5th level
						$fifth_level = DirectReferrals::where('recruitee_id', '=', $fourth_level -> recruiter_id) -> first();
						if ($fifth_level) {
							$ulvl[4]['user_id'] = $fifth_level -> recruiter_id;

							//6th level
							$sixth_level = DirectReferrals::where('recruitee_id', '=', $fifth_level -> recruiter_id) -> first();
							if ($sixth_level) {
								$ulvl[5]['user_id'] = $sixth_level -> recruiter_id;

								//7th level
								$seventh_level = DirectReferrals::where('recruitee_id', '=', $sixth_level -> recruiter_id) -> first();
								if ($seventh_level) {
									$ulvl[6]['user_id'] = $seventh_level -> recruiter_id;

									//8th level
									$eight_level = DirectReferrals::where('recruitee_id', '=', $seventh_level -> recruiter_id) -> first();
									if ($eight_level) {
										$ulvl[7]['user_id'] = $eight_level -> recruiter_id;

										//9th level
										$nineth_level = DirectReferrals::where('recruitee_id', '=', $eight_level -> recruiter_id) -> first();
										if ($nineth_level) {
											$ulvl[8]['user_id'] = $nineth_level -> recruiter_id;

											//10th level
											$tenth_level = DirectReferrals::where('recruitee_id', '=', $nineth_level -> recruiter_id) -> first();
											if ($tenth_level) {
												$ulvl[9]['user_id'] = $tenth_level -> recruiter_id;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $ulvl;
	}

}
