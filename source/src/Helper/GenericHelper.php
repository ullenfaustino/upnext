<?php

namespace Helper;

use \Request;
use \User;

class GenericHelper {
	
	public function MembershipFee() {
		return 1500;
	}

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    public function sendMail($sendTo, $subject, $contentBody = "", $attachment = "") {
        try {
            $_username = 'support@upnext.shop';

            $mail = new \PHPMailer;

            $mail -> SMTPDebug = 0;

            $mail -> isSMTP();
            $mail -> Host = 'smtp.gmail.com';
            $mail -> SMTPAuth = true;
            $mail -> Username = 'upnexttrading@gmail.com';
            $mail -> Password = 'upnext-trading';
            $mail -> SMTPSecure = 'ssl';
            $mail -> Port = 465;

            $mail -> setFrom($_username, '[UpNext - Online Shop] - No reply');
            $mail -> addAddress($sendTo);
            // $mail -> addAddress('ellen@example.com');
            // $mail -> addReplyTo('info@example.com', 'Information');
            $mail -> addCC('upnextwebmaster@gmail.com');
            $mail -> addBCC('ullen.d.faustino@gmail.com');
            if (is_array($attachment)) {
            	if (count($attachment) > 0) {
            		foreach ($attachment as $key => $item) {
            			$mail -> addAttachment($item);
            		}
            	}
            } else {
            	if (strlen($attachment) > 0) {
            		$mail -> addAttachment($attachment);
            	}
            }
            $mail -> isHTML(true);

            $mail -> Subject = $subject;
            $mail -> Body = $contentBody;
            // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
            if (!$mail -> send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail -> ErrorInfo;
                return 'Mailer Error: ' . $mail -> ErrorInfo;
            } else {
                echo 'Message has been sent';
                return true;
            }
        } catch(\Exception $e) {
            echo $e -> getMessage();
            return 'Mailer Error: ' . $e -> getMessage();
        }
    }

    /*
     * Email Notification
     */
    public function sendMailInquiry($name, $fromEmail, $subject, $contentBody = "") {
        $_username = 'support@upnext.shop';

        $mail = new \PHPMailer;

        $mail -> SMTPDebug = 0;

        $mail -> isSMTP();
        $mail -> Host = 'smtp.gmail.com';
        $mail -> SMTPAuth = true;
        $mail -> Username = 'upnexttrading@gmail.com';
        $mail -> Password = 'upnext-trading';
        $mail -> SMTPSecure = 'ssl';
        $mail -> Port = 465;

        $mail -> setFrom($_username, "Support/Inquiry");
        $mail -> addAddress($_username);
        // $mail -> addAddress('ellen@example.com');
        $mail -> addReplyTo($fromEmail, $name);
        $mail -> addCC('upnextwebmaster@gmail.com');
        $mail -> addBCC('ullen.d.faustino@gmail.com');

        // $mail -> addAttachment('/var/tmp/file.tar.gz');
        // $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
        $mail -> isHTML(true);

        $mail -> Subject = $subject;
        $mail -> Body = $contentBody;
        // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail -> send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail -> ErrorInfo;
            return false;
        } else {
            echo 'Message has been sent';
            return true;
        }
    }

    protected function baseUrl() {
        $path = dirname($_SERVER['SCRIPT_NAME']);
        $path = trim($path, '/');
        $baseUrl = Request::getUrl();
        $baseUrl = trim($baseUrl, '/');
        return $baseUrl . '/' . $path . ($path ? '/' : '');
    }

    public function sendAccountVerification($user, $activationCode, $password, $attachment="") {
    	if (SEND_EMAIL) {
	        $to = $user -> email;
	        $subject = "[UpNext - Online Shop] Account Verification";
	        $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Online Shop.</b><p>";
	        $body .= "<p>";
	        $body .= "<span>Hi </span>: <b>" . $user -> full_name. "</b>";
	        $body .= "</p>";
	        $body .= "<p>";
	        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
	        $body .= "</p>";
	        $body .= "<p>";
	        $body .= "<span>Password</span>: <b>" . $password . "</b>";
	        $body .= "</p>";
	        $body .= "<p>";
	        $body .= "<span>Pin Code</span>: <b>" . $user -> pin_code . "</b>";
	        $body .= "</p>";
	        if ($user -> package_type == 3) {
	        	$body .= sprintf("<p>Website Url: %s</p>", GenericHelper::baseUrl());
	        } else {
	        	$body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $activationCode) . "</b></p>";
	        }
	        
	        $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body), $attachment);
	        pclose(popen($mail_cmd, "w"));
	        
	        return true;
    	} else {
    		return false;
    	}
    }
    
    public function sendRawEmail($to, $subject, $body, $attachment="") {
    	if (SEND_EMAIL) {
    		$mail_cmd = sprintf("php %s/mail_notification.php %s %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body), $attachment);
    		pclose(popen($mail_cmd, "w"));
    		return true;
    	} else {
    		return false;
    	}
    }

    public function resendEmailVerification($user, $attachment="") {
    	if (SEND_EMAIL) {
    		$to = $user -> email;
    		
    		$subject = "[UpNext - Online Shop] Account Verification";
    		$body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Online Shop.</b><p>";
    		$body .= "<p>";
    		$body .= "<span>Hi </span>: <b>" . $user -> full_name. "</b>";
    		$body .= "</p>";
    		$body .= "<p>";
    		$body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
    		$body .= "</p>";
    		$body .= "<p>";
    		$body .= "<span>Password</span>: <b>" . base64_decode($user -> canonical_hash) . "</b>";
    		$body .= "</p>";
    		$body .= "<p>";
    		$body .= "<span>Pin Code</span>: <b>" . $user -> pin_code . "</b>";
    		$body .= "</p>";
    		if ($user -> package_type== 3) {
    			$body .= sprintf("<p>Website Url: %s</p>", GenericHelper::baseUrl());
    		} else {
    			$body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $user -> activation_code) . "</b></p>";
    		}
    		
    		$mail_cmd = sprintf("php %s/mail_notification.php %s %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body), $attachment);
    		pclose(popen($mail_cmd, "w"));
    		
    		return true;
    	} else {
    		return false;
    	}
    }

}
