<?php

namespace Helper;

class CodeGenerator {

    private function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0)
            return $min;
        $log = log($range, 2);
        $bytes = (int)($log / 8) + 1;
        $bits = (int)$log + 1;
        $filter = (int)(1<<$bits) - 1;
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter;
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public function getToken($length = 10) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnpqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        return $token;
    }
    
    public function getUsername($length = 10) {
    	$token = "";
    	$codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
    	$codeAlphabet .= "0123456789";
    	for ($i = 0; $i < $length; $i++) {
    		$token .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
    	}
    	return $token;
    }

    public function getCodes() {
        $year = date('Y');
        $md = date('md');
        $time = date('His');

        $token1 = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnpqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < 6; $i++) {
            $token1 .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        
        $token2 = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnpqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < 6; $i++) {
            $token2 .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        
        $token3 = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < 4; $i++) {
            $token3 .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        return sprintf("%s-%s-%s", $year . $token1, $md . $token2, $time . $token3);
    }

    public function getReferenceCode() {
        $token1 = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnpqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < 4; $i++) {
            $token1 .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        $token2 = "";
        $codeAlphabet = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnpqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < 4; $i++) {
            $token2 .= $codeAlphabet[$this -> crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        return sprintf("%s%s%s", $token1, time(), $token2);
    }

}
