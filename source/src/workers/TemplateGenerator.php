<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class TemplateGenerator extends BaseController {
	
	public function generateVoucher($params, $src) {
		$params = json_decode ( base64_decode ( $params ) );
		
		$reference_code = $params -> reference_code;
		$transaction_code = $params -> transaction_code;
		$account_code = $params -> account_code;
		$account_name = $params -> account_name;
		$amount = $params -> amount;
		$date_issued = $params -> date_issued;
		$email = $params -> email;
		
		$public_dir = __DIR__ . "/../../public/";
		$folder_dir = ($src === "PRODUCT_CLAIM_STUB.jpg") ? "assets/images/stub/" . $reference_code : "assets/images/vouchers/" . $reference_code;
		
		$vouchers_dir = $public_dir . $folder_dir;
		if (! file_exists ( $vouchers_dir )) {
			mkdir ( $vouchers_dir, 0777, TRUE );
		}
		
		$jpg_image = imagecreatefromjpeg ( __DIR__ . "/../../public/assets/images/templates/" . $src);
		$white = imagecolorallocate ( $jpg_image, 0, 0, 0 );
		$amount_color = ($src === "PRODUCT_CLAIM_STUB.jpg") ? imagecolorallocate ( $jpg_image, 0, 128, 0 ) : imagecolorallocate ( $jpg_image, 128, 0, 0 );
		$font_path = __DIR__ . "/../../public/assets/fonts/arial.ttf";
		
		$label = sprintf("%s | %s | %s | %s", $transaction_code, $account_code, $account_name, date ("F j, Y", strtotime($date_issued)));
		
		imagettftext ( $jpg_image, 17, 0, 50, 78, $white, $font_path, $label);
		
		if ($src === "PRODUCT_CLAIM_STUB.jpg") {
			imagettftext ( $jpg_image, 63, 0, 1300, 290, $amount_color, $font_path, $amount);
		} else {
			imagettftext ( $jpg_image, 63, 0, 1350, 285, $amount_color, $font_path, $amount);
		}
		
		$file_name = ($src === "PRODUCT_CLAIM_STUB.jpg") ? sprintf("%s_claim_stub.jpg", $account_code) : sprintf("%s_voucher.jpg", $account_code);
		$save_voucher = sprintf ( "%s/%s", $vouchers_dir, $file_name);
		imagejpeg ( $jpg_image, $save_voucher );
		
		$voucher = Vouchers::where("ref_code", "=", $transaction_code) -> first();
		if ($voucher) {
			$voucher -> image_path = sprintf("%s/%s", $folder_dir, $file_name);
			$voucher -> save();
		}
		
		// send to email
		$subject = ($src === "PRODUCT_CLAIM_STUB.jpg") ? "[Upnext - Online Shop] Product Claiming Stub" : "[Upnext - Online Shop] Voucher";
		$body = sprintf("<h1>Congratulations %s !</h1>", $account_name);
		GenericHelper::sendRawEmail($email, $subject, $body, $save_voucher);
	}
	
}

$generator = new TemplateGenerator();
$src = "PRODUCT_CLAIM_STUB.jpg";
if ($argv[1] ===  "voucher") {
	$src = "UPNEXT_VOUCHER.jpg";
}
$generator -> generateVoucher($argv[2], $src);

?>