<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class ExitManager extends BaseController {
	
	public function processExitBonus($table, $BonusAmount) {
		$hierarchies = UsersHasHierarchySiblings::where('table', '=', $table) -> groupBy('user_id') -> get();
		
		foreach ($hierarchies as $key => $h) {
			$leftCount = 0;
			$rightCount = 0;
			$siblings = UsersHasHierarchySiblings::siblings($h -> user_id, $table) -> get();
			
			$curr_user = Users::find($h -> user_id);
			
			echo "\n" . $h -> user_id;
			foreach ($siblings as $key => $sibling) {
				echo " [" . $sibling -> position . "=>" . $sibling -> recruitee_id . "] ";
				if ($sibling -> position == 0) {
					$leftCount++;
					$leftCount += BonusManager::getLineCount($sibling -> recruitee_id, $table);
				} else if ($sibling -> position == 1) {
					$rightCount++;
					$rightCount += BonusManager::getLineCount($sibling -> recruitee_id, $table);
				}
			}
			
			$earnings = $BonusAmount - 3000;
			
			switch ((int)$table) {
				case 1 :
					$hasExitRecordTable1 = ExitHistory::where('user_id', '=', $h -> user_id) -> where('table', '=', $table) -> get();
					
					if (count($hasExitRecordTable1) < 1) {
						if ($leftCount >= 3 && $rightCount >= 3) {//exit on level 1
							// if($h->user_id == 15) {
							echo "\n User Exit: " . $h -> user_id;
							
							$exit = new ExitHistory();
							$exit -> user_id = $h -> user_id;
							$exit -> table = $table;
							$exit -> value = $earnings;
							
							if ($exit -> save()) {
								BonusManager::updateUserBalance($h -> user_id, $earnings, $table);
								
								$hierarchy = new HierarchySiblings();
								$hierarchy -> recruitee_id = $h -> user_id;
								
								$placement = BonusManager::getPosition(2);
								$hierarchy -> position = (strcasecmp($placement['Position'], "Left") == 0) ? 0 : 1;
								if ($hierarchy -> save()) {
									$hasSiblings = new UsersHasHierarchySiblings();
									$hasSiblings -> user_id = $placement['Head'];
									$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
									$hasSiblings -> table = 2;
									if ($hasSiblings -> save(array("timestamps" => false))) {
										$this-> processExitBonus(2, 8000);
										
										$this -> sendExitNotification(json_encode($exit), $h -> user_id);
									} else {
										echo "unable to save user has heirarchy";
									}
								} else {
									echo "unable to save heirarchy";
								}
							}
						}
					}
					break;
				case 2 :
					$hasExitRecordTable2 = ExitHistory::where('user_id', '=', $h -> user_id) -> where('table', '=', $table) -> get();
					if (count($hasExitRecordTable2) < 1) {
						if ($leftCount >= 3 && $rightCount >= 3) {
							// if($h->user_id == 15) {
							echo "\n User Exit: " . $h -> user_id;
							
							$exit = new ExitHistory();
							$exit -> user_id = $h -> user_id;
							$exit -> table = $table;
							$exit -> value = $earnings;
							
							if ($exit -> save()) {
								BonusManager::updateUserBalance($h -> user_id, $earnings, $table);
								
								if ($curr_user -> matrix_count > 2) {
									$hierarchy = new HierarchySiblings();
									$hierarchy -> recruitee_id = $h -> user_id;
									
									$placement = BonusManager::getPosition(3);
									$hierarchy -> position = (strcasecmp($placement['Position'], "Left") == 0) ? 0 : 1;
									if ($hierarchy -> save()) {
										$hasSiblings = new UsersHasHierarchySiblings();
										$hasSiblings -> user_id = $placement['Head'];
										$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
										$hasSiblings -> table = 3;
										if ($hasSiblings -> save(array("timestamps" => false))) {
											$this-> processExitBonus(3, 8000);
											
											$this -> sendExitNotification(json_encode($exit), $h -> user_id);
										} else {
											echo "unable to save user has heirarchy";
										}
									} else {
										echo "unable to save heirarchy";
									}
								}
							}
						}
					}
					break;
				case 3 :
					$hasExitRecordTable3 = ExitHistory::where('user_id', '=', $h -> user_id) -> where('table', '=', $table) -> get();
					if (count($hasExitRecordTable3) < 1) {
						if ($leftCount >= 3 && $rightCount >= 3) {
							// if($h->user_id == 15) {
							echo "\n User Exit: " . $h -> user_id;
							
							$exit = new ExitHistory();
							$exit -> user_id = $h -> user_id;
							$exit -> table = $table;
							$exit -> value = $earnings;
							
							if ($exit -> save()) {
								BonusManager::updateUserBalance($h -> user_id, $earnings, $table);
								
								if ($curr_user -> matrix_count > 3) {
									$hierarchy = new HierarchySiblings();
									$hierarchy -> recruitee_id = $h -> user_id;
									
									$placement = BonusManager::getPosition(4);
									$hierarchy -> position = (strcasecmp($placement['Position'], "Left") == 0) ? 0 : 1;
									if ($hierarchy -> save()) {
										$hasSiblings = new UsersHasHierarchySiblings();
										$hasSiblings -> user_id = $placement['Head'];
										$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
										$hasSiblings -> table = 4;
										if ($hasSiblings -> save(array("timestamps" => false))) {
											$this-> processExitBonus(4, 8000);
											
											$this -> sendExitNotification(json_encode($exit), $h -> user_id);
										} else {
											echo "unable to save user has heirarchy";
										}
									} else {
										echo "unable to save heirarchy";
									}
								}
							}
						}
					}
					break;
				case 4 :
					$hasExitRecordTable4 = ExitHistory::where('user_id', '=', $h -> user_id) -> where('table', '=', $table) -> get();
					if (count($hasExitRecordTable4) < 1) {
						if ($leftCount >= 3 && $rightCount >= 3) {
							// if($h->user_id == 15) {
							echo "\n User Exit: " . $h -> user_id;
							
							$exit = new ExitHistory();
							$exit -> user_id = $h -> user_id;
							$exit -> table = $table;
							$exit -> value = $earnings;
							
							if ($exit -> save()) {
								BonusManager::updateUserBalance($h -> user_id, $earnings, $table);
								
								if ($curr_user -> matrix_count > 4) {
									$hierarchy = new HierarchySiblings();
									$hierarchy -> recruitee_id = $h -> user_id;
									
									$placement = BonusManager::getPosition(5);
									$hierarchy -> position = (strcasecmp($placement['Position'], "Left") == 0) ? 0 : 1;
									if ($hierarchy -> save()) {
										$hasSiblings = new UsersHasHierarchySiblings();
										$hasSiblings -> user_id = $placement['Head'];
										$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
										$hasSiblings -> table = 5;
										if ($hasSiblings -> save(array("timestamps" => false))) {
											$this-> processExitBonus(5, 8000);
											
											$this -> sendExitNotification(json_encode($exit), $h -> user_id);
										} else {
											echo "unable to save user has heirarchy";
										}
									} else {
										echo "unable to save heirarchy";
									}
								}
							}
						}
					}
					break;
				case 5 :
					$hasExitRecordTable5 = ExitHistory::where('user_id', '=', $h -> user_id) -> where('table', '=', $table) -> get();
					if (count($hasExitRecordTable5) < 1) {
						if ($leftCount >= 3 && $rightCount >= 3) {
							// if($h->user_id == 15) {
							echo "\n User Exit: " . $h -> user_id;
							
							$exit = new ExitHistory();
							$exit -> user_id = $h -> user_id;
							$exit -> table = $table;
							$exit -> value = $earnings;
							
							if ($exit -> save()) {
								BonusManager::updateUserBalance($h -> user_id, $earnings, $table);
								// end of process
								$this -> sendExitNotification(json_encode($exit), $h -> user_id);
							}
						}
					}
					break;
			}
		}
	}
	
	protected function sendExitNotification($exit_history, $user_id) {
		$user = Users::find($user_id);
		$to = $user -> email;
		$subject = "[UpNext - Online Shop] Platform Graduate";
		$body = "<p style='color: blue; font-weight: bold;'><b>Exit History Sample Response</b><p>";
		$body .= "<p>";
		$body .= "<span>Exit Sample Response: </span>: <b>" . $exit_history. "</b>";
		$body .= "</p>";
		
		$mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
		pclose(popen($mail_cmd, "w"));
	}
		
}

$manager = new ExitManager();
$manager-> processExitBonus($argv[1], $argv[2]);

?>