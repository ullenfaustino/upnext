<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class MAILNOTIFICATION extends BaseController {

    function __construct() {}

    public function SendMail($to, $subject, $body, $attachment="") {
    	$subject = base64_decode($subject);
    	$body = base64_decode($body);
    	GenericHelper::sendMail($to, $subject, $body, $attachment);
	}

}

$notif = new MAILNOTIFICATION();
$file = "";
try {
	$file = $argv[4];
} catch (\Exception $e) {
	$file = "";
}
$notif -> SendMail($argv[1], $argv[2], $argv[3], $file);

?>