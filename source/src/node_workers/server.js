const http = require('http').Server();
const io = require('socket.io')(http);
const port = process.env.PORT || 8888;
const redis = require('redis');
const subscriber = redis.createClient();
const request = require('request');
const cmd = require('node-cmd');

io.on('connection', function(socket) {
	socket.on('worker_message', function(msg) {
		console.log('message', msg);
		io.emit('worker_message', msg);
	});
});

getCurrency();
setInterval(function(){
	getCurrency();
}, 5000);

setInterval(function() {
	var command = "php backup_db.php";
	cmd.get(command, function(data) {
		console.log("[Command Response]|---> ", data);
	});
}, 30000);

function getCurrency() {
	var bitcoin_currency_url = "http://api.coindesk.com/v1/bpi/currentprice.json";
	request(bitcoin_currency_url, function (error, response, body) {
		var json = JSON.parse(body);
		var usd = json.bpi.USD;
		
		request('http://api.fixer.io/latest?base=USD', function (error, response, body) {
			  var rates = JSON.parse(body);
			  var php = rates.rates.PHP;
			  var bitcoin_rate = usd.rate_float * php;
//			  console.log(bitcoin_rate.toLocaleString());
			  var a = {
					  "bitcoin_rate" : Math.round(bitcoin_rate * 100) / 100,
					  "bitcoin" : bitcoin_rate.toLocaleString()
			  };
			  io.emit("current_bitcoin_rate", a);
		});
	});
}

subscriber.on('message', function(channel, payload) {
	console.log(payload);
	var json = {};
	try {
		json = JSON.parse(payload);
	} catch (e) {
	}

	io.emit('worker_message', json.message);
});
subscriber.subscribe('notification');

http.listen(port, function() {
	console.log('listening on *:' + port);
});
