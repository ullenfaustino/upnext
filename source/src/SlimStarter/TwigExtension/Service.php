<?php

namespace SlimStarter\TwigExtension;
use \Slim;

use \Sentry;
use \User;
use \Users;
use \DirectReferrals;

class Service extends \Twig_Extension
{
    public function getName()
    {
        return 'service';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserdetails', array($this, 'getUserdetails')),
        	new \Twig_SimpleFunction('getDirect', array($this, 'getDirect')),
        	new \Twig_SimpleFunction('getAvatarPath', array($this, 'getAvatarPath')),
        	new \Twig_SimpleFunction('getUserIconColor', array($this, 'getUserIconColor')),
        );
    }

	public function getAvatarPath() {
		return AVATAR_PATH;
	}

    public function getUserdetails($id) {
    	return User::getUserDetails($id)->first();
    }
    
    public function getDirect($id) {
    	return DirectReferrals::getDirect($id)->first();
    }
    
    public function getUserIconColor($userId) {
        $user = Users::find($userId);
        $color = "geneology/silver-gene.png";
        if ($user) {
            $color = "geneology/black-gene.png";
        }
        return $color;
    }
}
