<?php

namespace Geneology\Controllers;

use \User;
use \Users;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \DirectReferrals;
use \Codes;
use \ExitHistory;
use \CodeGenerator;
use \BonusManager;
use \GeneralSettings;
use \GenericHelper;
use \Vouchers;
use \Subscription;

use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class GeneologyController extends BaseController {
	public function __construct() {
		parent::__construct ();
		$this->data ['active_menu'] = 'geneology';
	}
	
	/**
	 * display the admin dashboard
	 */
	public function index($user_id = 1) {
// 		$user = Sentry::getUser ();
		$user = Users::where("id","=",$user_id)
						-> orWhere("ref_code","=",$user_id)
						-> first();
		
		$this->data ['title'] = 'Genealogy';
		// set user id when item cliked
		$id = ($user) ? $user->id : $user_id;
		$this->data ['dynamic_user'] = Sentry::findUserById ( $id );
		
		$this -> getTables($id);
		
		$this->data ['user_id'] = $id;
		
		App::render ( '@geneology/index.twig', $this->data );
	}
	
	private function getTables($id) {
		$tables = [];
		array_push($tables, 1);
		
		$hasExitRecordTable1 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 1 )->first ();
		if ($hasExitRecordTable1) {
			$tables = [];
			array_push($tables, 1);
			array_push($tables, 2);
		}
		
		$hasExitRecordTable2 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 2 )->first ();
		if ($hasExitRecordTable2) {
			$tables = [];
			array_push($tables, 1);
			array_push($tables, 2);
			array_push($tables, 3);
		}
		
		$hasExitRecordTable3 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 3 )->first ();
		if ($hasExitRecordTable3) {
			$tables = [];
			array_push($tables, 1);
			array_push($tables, 2);
			array_push($tables, 3);
			array_push($tables, 4);
		}
		
		$hasExitRecordTable4 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 4 )->first ();
		if ($hasExitRecordTable4) {
			$tables = [];
			array_push($tables, 1);
			array_push($tables, 2);
			array_push($tables, 3);
			array_push($tables, 4);
			array_push($tables, 5);
		}
		
		$hasExitRecordTable5 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 5 )->first ();
		if ($hasExitRecordTable5) {
			$tables = [];
			array_push($tables, 1);
			array_push($tables, 2);
			array_push($tables, 3);
			array_push($tables, 4);
			array_push($tables, 5);
		}
		
		$this->data ['table'] = $tables[count($tables) - 1];
		$this->data ['tables'] = $tables;
	}
	
	public function accountRegistration() {
		$user = Sentry::getUser ();
		
		$id = ($user->is_registered == 1) ? $user->id : 6;
		$this->data ['dynamic_user'] = $id;
		
		$this->data ['title'] = 'Geneology Registration';
		App::render ( '@geneology/account_activation.twig', $this->data );
	}
	public function registerMemberOnGeneology() {
		$request_uri = Input::post("request_uri");
		$position = Input::post ( "position" );
		$head_ref_code = Input::post ( "head_ref_code" );
		$activation_code = Input::post ( "activation_code" );
		$direct_referral = Input::post ( "direct_referral" );
		
		try {
			$user = Sentry::getUser ();
			
			$pass_code = Codes::where ( "generated_code", "=", $activation_code )
			                 ->where ( "is_used", "=", 0 )
			                 ->first ();
			if ($pass_code) {
				$dr_id = 6;
				$direct = Users::where ( "username", "=", $direct_referral )->first ();
				if ($direct) {
					$dr_id = $direct->id;
				} else {
					App::flash ( 'message_status', false );
					App::flash ( 'message', 'Invalid Sponsor Username!' );
					Response::redirect ( $this->siteUrl ( $request_uri) );
					return;
				}
					
				$DR = Sentry::findUserById ( $dr_id );
				$head = Users::where ( "ref_code", "=", $head_ref_code )->first ();
				if ($head) {
					$hierarchy = new HierarchySiblings ();
					$hierarchy->recruitee_id = $user->id;
					$hierarchy->position = (strcasecmp ( $position, "Left" ) == 0) ? 0 : 1;
					
					if ($hierarchy->save ()) {
						$hasSiblings = new UsersHasHierarchySiblings ();
						$hasSiblings->user_id = $head->id;
						$hasSiblings->hierarchy_sibling_id = $hierarchy->id;
						$hasSiblings->table = 1;
						$hasSiblings->save ( array (
								"timestamps" => false 
						) );
						
						// set direct referral
                        $dr_amount = 100;
                        $dr_bonus = GeneralSettings::where('module_name', '=', 'dr_bonus') -> first();
                        if ($dr_bonus) {
                            $c = json_decode($dr_bonus -> content);
                            $dr_amount = $c -> value;
                        }
						$referral = new DirectReferrals ();
						$referral->recruiter_id = $DR->id;
						$referral->recruitee_id = $user->id;
						$referral->dr_earnings = $dr_amount;
						$referral->save ();
						
						$pass_code->user_id = $user->id;
						$pass_code->is_used = 1;
						$pass_code->save ();
						
						$user->is_registered = 1;
						$user->save ();
						
						BonusManager::setCompanyAllocation($user->id);
						BonusManager::processExitBonus ( 1, 4000 );
					}
				} else {
					App::flash ( 'message_status', false );
					App::flash ( 'message', 'Invalid Head on Geneology table.' );
				}
			} else {
				App::flash ( 'message_status', false );
				App::flash ( 'message', 'Code already in used.' );
			}
		} catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
			App::flash ( 'message_status', false );
			App::flash ( 'message', 'User was not found.' );
		}
		Response::redirect ( $this->siteUrl ($request_uri) );
	}
	
	public function addNewMember() {
	    $curr_user = Sentry::getUser();
	    
	    $codeGen = new CodeGenerator();
        
	    $username = "UN" . $codeGen -> getUsername(5);
	    $pin_code = $codeGen -> getToken(4);
        $full_name = Input::post("full_name");
        $email = Input::post("email");
        $activation_code = Input::post("activation_code");
        $position = Input::post ( "position" );
        $head_ref_code = Input::post ( "head_ref_code" );
        $package_type = Input::post("package_type");
        $direct_referral = Input::post ( "direct_referral" );
        
        try {
            $pass_code = Codes::where ( "generated_code", "=", $activation_code )
                             ->where ( "is_used", "=", 0 )
                             ->first ();
            if ($pass_code) {
            	$dr_id = 6;
            	$dr_user = Users::where("username","=",$direct_referral) -> first();
            	if ($dr_user) {
            		$dr_id = $dr_user -> id;
            	}
            	$DR = Sentry::findUserById ( $dr_id );
                $head = Users::where ( "ref_code", "=", $head_ref_code )->first ();
                if ($head) {
                    $randomPassword = $codeGen -> getToken(12);
                    $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
                                    'username' => trim($username), 
                                    'email' => trim($email), 
                                    'full_name' => trim($full_name), 
                                    'password' => $randomPassword, 
                                    'canonical_hash' => base64_encode($randomPassword),
                    				'pin_code' => $pin_code, 
                                    'activated' => 0, 
                                    'is_registered' => 1, 
                    				'is_for_activation' => 2, 
                    				'package_type' => $package_type, 
                                    'permissions' => array('member' => 1),
                                    'user_type' => 2
                                   ];
                    $user = Sentry::createUser($credential);
                    $user -> addGroup(Sentry::getGroupProvider() -> findByName('Members'));
                    
                    $hierarchy = new HierarchySiblings ();
                    $hierarchy->recruitee_id = $user->id;
                    $hierarchy->position = (strcasecmp ( $position, "Left" ) == 0) ? 0 : 1;
                    
                    if ($hierarchy->save ()) {
                        $hasSiblings = new UsersHasHierarchySiblings ();
                        $hasSiblings->user_id = $head->id;
                        $hasSiblings->hierarchy_sibling_id = $hierarchy->id;
                        $hasSiblings->table = 1;
                        $hasSiblings->save ( array (
                                "timestamps" => false 
                        ) );
                        
                        // set direct referral
                        $dr_amount = 100;
                        $dr_bonus = GeneralSettings::where('module_name', '=', 'dr_bonus') -> first();
                        if ($dr_bonus) {
                            $c = json_decode($dr_bonus -> content);
                            $dr_amount = $c -> value;
                        }
                        $referral = new DirectReferrals ();
                        $referral->recruiter_id = $DR->id;
                        $referral->recruitee_id = $user->id;
                        $referral->dr_earnings = $dr_amount;
                        $referral->save ();
                        
                        $pass_code->user_id = $user->id;
                        $pass_code->is_used = 1;
                        $pass_code->save ();
                        
                        BonusManager::setCompanyAllocation($user->id);
                        BonusManager::processExitBonus ( 1, 4000 );
                        
                        $activationCode = $user -> getActivationCode();
                        GenericHelper::sendAccountVerification($user, $activationCode, $randomPassword);
                        
                        if ($user -> package_type == 1) {
                        	$expiration = new \DateTime('now');
                        	$expiration-> modify('+1 year');
                        	$expiration_date = $expiration-> format('Y-m-d H:i:s');
                        	
                        	$subscription = new Subscription();
                        	$subscription -> user_id = $user -> id;
                        	$subscription -> expiration_date = $expiration_date;
                        	$subscription -> is_active = 1;
                        	$subscription -> save();
                        }
                        
                        if ($user -> package_type == 2) {
                        	$voucher = new Vouchers();
                        	$voucher -> ref_code = "UPNXT2-" . $codeGen-> getToken(8);
                        	$voucher -> user_id = $user -> id;
                        	$voucher -> voucher_amount = 750;
                        	$voucher -> is_voucher = 0;
                        	$voucher -> save();
                        	
                        	$params["reference_code"] = $user -> ref_code;
                        	$params["transaction_code"] = $voucher -> ref_code;
                        	$params["account_code"] = $user -> username;
                        	$params["account_name"] = $user -> full_name;
                        	$params["amount"] = $voucher -> voucher_amount;
                        	$params["date_issued"] = date("F j, Y", strtotime ( $voucher->created_at ));
                        	$params["email"] = $user -> email;
                        	$this -> generateOnTemplate($params, "stub");
                        }
                    }
                    
                    App::flash ( 'message_status', true );
                    App::flash ( 'message', 'New account successfully added.' );
                } else {
                    App::flash ( 'message_status', false );
                    App::flash ( 'message', 'Invalid Head on Geneology table.' );
                }
            } else {
                App::flash ( 'message_status', false );
                App::flash ( 'message', 'Code already in used.' );
            }
        } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
            App::flash('message_status', false);
            App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
        } catch ( \Exception $e ) {
            App::flash('message_status', false);
            App::flash('message', "[Exception:]" . $e -> getMessage());
        }
        Response::redirect($this -> siteUrl('/member/geneology/' . $curr_user -> ref_code));
	}
}
