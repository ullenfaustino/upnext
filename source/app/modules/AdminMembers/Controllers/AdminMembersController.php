<?php

namespace AdminMembers\Controllers;

use \App;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Response;
use \Admin\BaseController;

use \GenericHelper;
use \DirectReferrals;

class AdminMembersController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_member_accounts';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Members Account';
		
		App::render('@adminmembers/index.twig', $this -> data);
	}
	
	public function updateLeaderStatus() {
		$member_id = Input::post("member_id");
		
		$member = Users::find($member_id);
		if ($member -> is_leader == 0) {
			$member -> is_leader = 1;
		} else {
			$member -> is_leader = 0;
		}
		$member -> save();
		
		Response::setBody(json_encode($member));
	}
	
	public function resendEmailConfirmation() {
		$user_id = Input::post("user_id");
		$user = Users::find($user_id);
		
		GenericHelper::resendEmailVerification($user);
		
		App::flash("message_status", true);
		App::flash("message", "Email successfully sent.");
		Response::redirect($this -> siteUrl("admin/accounts"));
	}
	
	public function paginatedList() {
		$draw = Input::post ( "draw" );
		$limit = Input::post ( "length" );
		$start = Input::post ( "start" );
		$current_page = ceil ( ($start + 1) / 10 );
		$searchValue = trim ( Input::post ( "search" ) ['value'], '"' );
		$order = Input::post ( "order" ) [0] ['column'];
		$dir = Input::post ( "order" ) [0] ['dir'];
		$hasSearchKeyword = strlen ( $searchValue ) > 0;
		
		$select = array (
			"*"
		);
		
		$accounts = Users::select ( $select )
// 						-> where("is_registered","=",1)
// 						-> where("is_for_activation","=",2)
						-> where("user_type","=",2);
		
		$accountsFiltered = Users::select ( $select )
// 								-> where("is_registered","=",1)
// 								-> where("is_for_activation","=",2)
								-> where("user_type","=",2)
								-> where ( function ($query) use ($searchValue) {
									return $query->where ( 'users.ref_code', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.username', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.full_name', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.email', 'LIKE', '%' . $searchValue . '%' );
								});
			
		$totalCount = $accounts->count ();
		$filteredCount = $accountsFiltered->count ();
		switch ($order) {
			case '0' :
				$col = 'users.id';
				break;
			case '1' :
				$col = 'users.ref_code';
				break;
			case '2' :
				$col = 'users.username';
				break;
			case '3' :
				$col = 'users.full_name';
				break;
			case '4' :
				$col = 'users.id';
				break;
			case '5' :
				$col = 'users.email';
				break;
			case '6' :
				$col = 'users.package_type';
				break;
			case '7' :
				$col = 'users.created_at';
				break;
			case '8' :
				$col = 'users.activated';
				break;
			case '9' :
				$col = 'users.created_at';
				break;
			default :
				$col = 'users.created_at';
		}
		
		$accountsData = $accountsFiltered ->orderby ( $col, $dir )->offset ( $start )->take ( $limit )->get ();
		
		$arrData = [];
		foreach ( $accountsData as $key => $value ) {
			$is_checked = ($value -> is_leader == 1) ? "checked" : "";
			$action = "";
			
			$email_notif = "<form method='post' action='/admin/accounts/resend/confirmation'>";
			$email_notif .= sprintf("<input type='hidden' name='user_id' value='%s'>", $value->id);
			$email_notif .= "<button type='submit' class='btn btn-default waves-effect waves-float'>Resend Email Confirmation</button>";
			$email_notif .= "</form>";
			
			// set Sponsor
			$dr_name = "";
			$dr = DirectReferrals::where("recruitee_id", "=", $value->id) -> first();
			if ($dr) {
				$dr_user = Users::find($dr -> recruiter_id);
				if ($dr_user) {
					$dr_name = sprintf("%s | %s", $dr_user -> username, $dr_user -> full_name);
				}
			}
			
			$arrData [] = [
				$value->id,
				$value->ref_code,
				$value->username,
				$value->full_name,
				$dr_name,
				$value->email,
				"Option " . $value->package_type,
				(($value -> package_type != 3) ? $value -> matrix_count : "n/a"),
				sprintf("<input type='checkbox' class='is-leader-check' value='%s' %s %s>", $key, $is_checked, (($value -> activated == 1 && $value -> is_for_activation == 2) ? "" : "disabled")),
				(($value -> activated == 1) ? "Activated" : "Not Activated"),
				((strlen($value -> activation_code) > 0)) ? $email_notif : "",
				date ( "F j, Y", strtotime ( $value->created_at ) ),
			];
		}
		
		$response ['draw'] = $draw;
		$response ['recordsTotal'] = $totalCount;
		$response ['recordsFiltered'] = $filteredCount;
		$response ['data'] = $arrData;
		$response ['zorder'] = $order;
		$response ['zdir'] = $dir;
		$response ['current_page'] = $current_page;
		$response ['search'] = $searchValue;
		$response ['raw_data'] = $accountsData;
		
		Response::headers ()->set ( 'Content-Type', 'application/json' );
		Response::setBody ( json_encode ( $response ) );
	}
}
