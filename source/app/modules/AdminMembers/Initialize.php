<?php

namespace AdminMembers;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminMembers';
	}

	public function getModuleAccessor() {
		return 'adminmembers';
	}

	public function registerAdminRoute() {
		Route::resource('/accounts', 'AdminMembers\Controllers\AdminMembersController');
		
		Route::post('/accounts/paginated/list', 'AdminMembers\Controllers\AdminMembersController:paginatedList') -> name("admin_member_accounts");
		Route::post('/accounts/update/leader/status', 'AdminMembers\Controllers\AdminMembersController:updateLeaderStatus') -> name("admin_update_leader_status");
		Route::post('/accounts/resend/confirmation', 'AdminMembers\Controllers\AdminMembersController:resendEmailConfirmation');
	}

}
