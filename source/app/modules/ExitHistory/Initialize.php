<?php

namespace ExitHistory;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'ExitHistory';
	}

	public function getModuleAccessor() {
		return 'exithistory';
	}

	public function registerPublicRoute() {
		Route::resource('/exit_history', 'ExitHistory\Controllers\ExitHistoryController');
		
		Route::post('/exit_history/paginated/list', 'ExitHistory\Controllers\ExitHistoryController:paginatedListExits') -> name("exit_paginated_list");
	}

}
