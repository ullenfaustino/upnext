<?php

namespace ExitHistory\Controllers;

use \User;
use \ExitHistory;

use \App;
use \Sentry;
use \Input;
use \Response;
use \Member\BaseController;

class ExitHistoryController extends BaseController {
	public function __construct() {
		parent::__construct ();
		$this->data ['active_menu'] = 'exit_history';
	}

	public function index() {
		$user = Sentry::getUser ();
		$this -> data["title"] = "Exit History";

		App::render ( '@exithistory/index.twig', $this->data );
	}

	public function paginatedListExits() {
		$user = Sentry::getUser();
		$draw = Input::post ( "draw" );
		$limit = Input::post ( "length" );
		$start = Input::post ( "start" );
		$current_page = ceil ( ($start + 1) / 10 );
		$searchValue = trim ( Input::post ( "search" ) ['value'], '"' );
		$order = Input::post ( "order" ) [0] ['column'];
		$dir = Input::post ( "order" ) [0] ['dir'];
		$hasSearchKeyword = strlen ( $searchValue ) > 0;
		$table = Input::post("table");

		$select = array (
			"*", "exit_history.id as id"
		);

		$exits = ExitHistory::select ( $select )
							-> leftJoin("users","users.id","=","exit_history.user_id")
							-> where("exit_history.user_id","=",$user -> id)
							-> where("table", "=", $table);

		$exitsFiltered = ExitHistory::select ( $select )
								-> leftJoin("users","users.id","=","exit_history.user_id")
								-> where("exit_history.user_id","=",$user -> id)
								-> where("table", "=", $table)
								-> where ( function ($query) use ($searchValue) {
									return $query->where ( 'users.ref_code', 'LIKE', '%' . $searchValue . '%' )
									->orWhere ( 'users.username', 'LIKE', '%' . $searchValue . '%' )
									->orWhere ( 'users.full_name', 'LIKE', '%' . $searchValue . '%' );
								});

		$totalCount = $exits->count ();
		$filteredCount = $exitsFiltered->count ();
		switch ($order) {
			case '0' :
				$col = 'users.full_name';
				break;
			case '1' :
				$col = 'users.username';
				break;
			case '2' :
				$col = 'exit_history.value';
				break;
			default :
				$col = 'exit_history.created_at';
		}

		$exitsData = $exitsFiltered -> orderby($col, $dir) -> offset($start) -> take($limit) -> get();

		$arrData = [];
		foreach ( $exitsData as $key => $value ) {
			$arrData [] = [
				$value->full_name,
				$value->username,
				number_format ( $value->value, 2, '.', ',' ),
				date ( "F j, Y", strtotime ( $value->created_at ) ),
			];
		}

		$response ['draw'] = $draw;
		$response ['recordsTotal'] = $totalCount;
		$response ['recordsFiltered'] = $filteredCount;
		$response ['data'] = $arrData;
		$response ['zorder'] = $order;
		$response ['zdir'] = $dir;
		$response ['current_page'] = $current_page;
		$response ['search'] = $searchValue;
		$response ['raw_data'] = $exitsData;

		Response::headers ()->set ( 'Content-Type', 'application/json' );
		Response::setBody ( json_encode ( $response ) );
	}

}
