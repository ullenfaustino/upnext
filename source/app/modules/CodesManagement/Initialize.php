<?php

namespace CodesManagement;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'CodesManagement';
	}

	public function getModuleAccessor() {
		return 'codesmanagement';
	}

	// public function registerAdminMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/codes', 'CodesManagement\Controllers\CodesManagementController');
		
		Route::get('/codes/purchase/request', 'CodesManagement\Controllers\CodesManagementController:viewPurchaseCodesRequest');
		Route::get('/codes/purchase/:purchase_id/proofs', 'CodesManagement\Controllers\CodesManagementController:viewProofOfPayment');
		
		Route::post('/codes/purchase/approve', 'CodesManagement\Controllers\CodesManagementController:approvePurchaseRequest') -> name('approve_request');
	}

}
