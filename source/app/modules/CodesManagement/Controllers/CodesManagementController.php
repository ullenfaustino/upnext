<?php

namespace CodesManagement\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;

use \CodeGenerator;
use \PurchaseActivation;
use \PurchaseActivationProof;
use \Codes;

class CodesManagementController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'codes_management';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Codes Management';
		$this -> data['active_sub_menu'] = 'sub_used_unused_codes';

		$unused_codes = Codes::leftJoin("users as User", "User.id", "=", "codes.user_id")
		                      -> leftJoin("purchase_activation","purchase_activation.id","=","codes.purchase_activation_id") 
		                      -> where("is_used", "=", 0) 
		                      -> select(array("*", "codes.id as id", "purchase_activation.user_id as _user_id")) 
		                      -> get();
		$this -> data['unused_codes'] = $unused_codes;

		$used_codes = Codes::leftJoin("users as User", "User.id", "=", "codes.user_id") 
		                  -> leftJoin("purchase_activation","purchase_activation.id","=","codes.purchase_activation_id") 
		                  -> where("is_used", "=", 1) 
		                  -> select(array("*", "codes.id as id", "purchase_activation.user_id as _user_id")) 
		                  -> get();
		$this -> data['used_codes'] = $used_codes;

		App::render('@codesmanagement/index.twig', $this -> data);
	}

	public function viewPurchaseCodesRequest() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Codes Request';
		$this -> data['active_sub_menu'] = 'sub_purchase_codes';

		$purchase_requests = PurchaseActivation::leftJoin("users as User", "User.id", "=", "purchase_activation.user_id") 
											-> where("status", "=", 0) 
											-> select(array("*", "purchase_activation.id as id")) -> get();
		$this -> data['purchase_requests'] = $purchase_requests;

		App::render('@codesmanagement/purchase_codes.twig', $this -> data);
	}

	public function viewProofOfPayment($purchase_id) {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Proof Documents';

		$proofs = PurchaseActivationProof::where("purchase_activation_id", "=", $purchase_id) -> get();
		$this -> data['purchase_proofs'] = $proofs;

		App::render('@codesmanagement/purchase_proofs.twig', $this -> data);
	}

	public function approvePurchaseRequest() {
		$user = Sentry::getUser();
		$codeGenerator = new CodeGenerator();

		$purchase_id = Input::post("purchase_id");

		$purchase_code = PurchaseActivation::find($purchase_id);
		$purchase_code -> approved_by = $user -> id;
		$purchase_code -> status = 1;

		$requestor_id = $purchase_code -> user_id;
		for ($i = 0; $i < $purchase_code -> quantity; $i++) {
			$token = $codeGenerator -> getCodes();

			$code = new Codes();
			$code -> generated_code = $token;
			$code -> purchase_activation_id= $purchase_code -> id;
			// $code -> user_id = $requestor_id;
			$code -> save();
		}
		$purchase_code -> save();

		App::flash('message_status', true);
		App::flash('message', 'Transaction Completed.');
		Response::redirect($this -> siteUrl('admin/codes/purchase/request'));
	}

}
