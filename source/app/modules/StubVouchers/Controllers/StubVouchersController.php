<?php

namespace StubVouchers\Controllers;

use \User;
use \Vouchers;

use \App;
use \Sentry;
use \Member\BaseController;

class StubVouchersController extends BaseController {
	public function __construct() {
		parent::__construct ();
		$this->data ['active_menu'] = 'stub_vouchers';
	}
	
	public function index() {
		$user = Sentry::getUser ();
		
		$this -> data["title"] = "Vouchers";
		$this -> data["vouchers"] = Vouchers::where("user_id", "=", $user -> id) -> get();
		
		App::render ( '@stubvouchers/index.twig', $this->data );
	}
	
}
