<?php

namespace StubVouchers;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'StubVouchers';
	}

	public function getModuleAccessor() {
		return 'stubvouchers';
	}

	public function registerPublicRoute() {
		Route::resource('/vouchers', 'StubVouchers\Controllers\StubVouchersController');
	}

}
