<?php

namespace PurchaseActivationCode;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'PurchaseActivationCode';
	}

	public function getModuleAccessor() {
		return 'purchaseactivationcode';
	}

	public function registerPublicRoute() {
		Route::resource('/purchase/activation', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController');
		
		Route::get('/purchase/codes', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewPurchaseCodes');
		Route::get('/purchase/codes/:ref_code', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewAssociatedCodes');
		Route::get('/purchase/:ref_code/proofs', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewProofOfPayment');
		Route::get('/purchase/waiting/activation', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:waitingActivation');
		
		Route::post('/purchase/payment/cash', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:paymentActivation') -> name('payment_activation');
		Route::post('/purchase/paginated/list', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:paginatedList') -> name('activation_purchase_paginated_list');
	}

}
