<?php

namespace PurchaseActivationCode\Controllers;

use \User;
use \Banks;
use \App;
use \Input;
use \Sentry;
use \Response;
use \Member\BaseController;

use \GenericHelper;
use \PurchaseActivation;
use \PurchaseActivationProof;
use \CodeGenerator;
use \Codes;

class PurchaseActivationCodeController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'purchase_code';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		
		if ($user -> is_for_activation == 1 || $user -> is_for_activation == 2) {
			Response::Redirect($this -> siteUrl("member"));
		} else {
			$this -> data['title'] = 'Purchase Activation';
			$this -> data['membership_fee'] = GenericHelper::MembershipFee();
			$this -> data['is_registered'] = ($user -> is_registered == 1) ? TRUE : FALSE;
			$this -> data['banks'] = Banks::all();
			$this -> data["company_btc_wallet"] = "2N7t14vJE6KxGooZ2EeERpokzWzLRi8hyBu";
			
			App::render('@purchaseactivationcode/index.twig', $this -> data);
		}
	}
	
	public function waitingActivation() {
		$this -> data['title'] = 'Waiting for activation';
		App::render('@purchaseactivationcode/waiting_for_confirmation.twig', $this -> data);
	}

	public function viewPurchaseCodes() {
		$user = Sentry::getUser();
		if ($user -> is_leader == 1) {
			$this -> data['title'] = 'Purchase History';
			$this -> data['membership_fee'] = GenericHelper::MembershipFee();
			
			App::render('@purchaseactivationcode/purchase_codes.twig', $this -> data);
		} else {
			Response::redirect($this -> siteUrl("member"));
		}
	}
	
	public function viewAssociatedCodes($ref_code) {
		$purchase_activation = PurchaseActivation::where("reference_code","=",$ref_code) -> first();
		$codes = Codes::where("purchase_activation_id", "=", $purchase_activation -> id) -> get();
		
		$this -> data["ref_code"] = $ref_code;
		$this -> data["assoc_codes"] = $codes;
		
		App::render('@purchaseactivationcode/purchase_codes_list.twig', $this -> data);
	}
	
	public function paginatedList() {
		$user = Sentry::getUser();
		
		$draw = Input::post ( "draw" );
		$limit = Input::post ( "length" );
		$start = Input::post ( "start" );
		$current_page = ceil ( ($start + 1) / 10 );
		$searchValue = trim ( Input::post ( "search" ) ['value'], '"' );
		$order = Input::post ( "order" ) [0] ['column'];
		$dir = Input::post ( "order" ) [0] ['dir'];
		$hasSearchKeyword = strlen ( $searchValue ) > 0;
		
		$select = array (
				"*"
		);
		
		$purchases = PurchaseActivation::select ( $select )
										-> where("user_id", "=", $user -> id);
		
		$purchasesFiltered = PurchaseActivation::select ( $select )
												-> where("user_id", "=", $user -> id)
												-> where ( function ($query) use ($searchValue) {
													return $query -> where ( 'purchase_activation.reference_code', 'LIKE', '%' . $searchValue . '%' )
													-> orWhere ( 'purchase_activation.id', 'LIKE', '%' . $searchValue . '%' );
												});
			
		$totalCount = $purchases -> count();
		$filteredCount = $purchasesFiltered -> count();
		switch ($order) {
			case '0' :
				$col = 'purchase_activation.reference_code';
				break;
			case '1' :
				$col = 'purchase_activation.quantity';
				break;
			case '2' :
				$col = 'purchase_activation.total_amount';
				break;
			case '3' :
				$col = 'purchase_activation.btc_received_amount';
				break;
			case '4' :
				$col = 'purchase_activation.payment_method';
				break;
			case '5' :
				$col = 'purchase_activation.status';
				break;
			case '6' :
				$col = 'purchase_activation.remarks';
				break;
			case '7' :
				$col = '';
				break;
			default :
				$col = 'purchase_activation.created_at';
		}
		
		$purchasesData = $purchasesFiltered ->orderby ( $col, $dir )->offset ( $start )->take ( $limit )->get ();
		
		$arrData = [];
		foreach ( $purchasesData as $key => $value ) {
			$action = "";
			$action .= '<a href="/member/purchase/codes/' . $value->reference_code. '" target="_blank" title="View Associated Codes" class="btn btn-default btn-icon waves-effect waves-circle waves-float"><i class="fa fa-search" aria-hidden="true"></i></a>';
			$action .= '<a href="/member/purchase/' . $value->reference_code. '/proofs" target="_blank" title="View Proof of Payments" class="btn btn-default btn-icon waves-effect waves-circle waves-float"><i class="fa fa-credit-card" aria-hidden="true"></i></a>';
			
			$arrData [] = [
					$value->reference_code,
					$value->quantity,
					sprintf ( "<b>%s</b>", number_format ( $value->total_amount, 2, '.', ',' ) ),
					sprintf ( "<b>%s</b>", number_format ( $value->btc_received_amount, 8, '.', ',' ) ),
					(($value->payment_method == 1) ? "CASH" : (($value->payment_method == 2) ? "Bank Deposit" : (($value->payment_method == 3) ? "Remittance" : "Bitcoin Payment"))),
					(($value->status == 1) ? "Approved" : "Pending"),
					$value->remarks,
					sprintf("<span style='display: -webkit-inline-box;'>%s</span>", $action),
					date ( "F j, Y", strtotime ( $value->created_at ) ),
			];
		}
		
		$response ['draw'] = $draw;
		$response ['recordsTotal'] = $totalCount;
		$response ['recordsFiltered'] = $filteredCount;
		$response ['data'] = $arrData;
		$response ['zorder'] = $order;
		$response ['zdir'] = $dir;
		$response ['current_page'] = $current_page;
		$response ['search'] = $searchValue;
		$response ['raw_data'] = $purchasesData;
		
		Response::headers ()->set ( 'Content-Type', 'application/json' );
		Response::setBody ( json_encode ( $response ) );
	}

	public function viewProofOfPayment($ref_code) {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Proof Documents';

		$purchase_activation = PurchaseActivation::where("reference_code","=",$ref_code) -> first();
		$proofs = PurchaseActivationProof::where("purchase_activation_id", "=", $purchase_activation -> id) -> get();
		$this -> data['purchase_proofs'] = $proofs;

		App::render('@purchaseactivationcode/purchase_proofs.twig', $this -> data);
	}

	public function paymentActivation() {
		$user = Sentry::getUser();
		$codeGen = new CodeGenerator();
		
		$redirect = Input::post("redirect");
		$company_wallet_address = Input::post("company_wallet_address");
		$senderBitcoinAddress = Input::post("external_bitcoin_address");
		$btc_amount = Input::post("btc_amount");
		$remarks = Input::post("remarks");
		$quantity = (!is_null(Input::post("quantity")) && strlen(Input::post("quantity")) > 0) ? Input::post("quantity") : 1;
		$payment_method = Input::post("payment_method");
		$payment_proofs = Input::file("payment_proofs");
		
		if (count($payment_proofs["name"]) == 0 && $payment_method != 1) {
			App::flash('message_status', false);
			App::flash('message', 'No File Uploaded');
			Response::redirect($this -> siteUrl('member/purchase/activation'));
			return;
		}
		
		$amount = 0;
		if ($user -> package_type == 1 || $user -> package_type == 2) {
			$amount = 1500;
		} else {
			$amount = 500;
		}

		$totalAmount = $amount * $quantity;

		$purchase_code = new PurchaseActivation();
		$purchase_code -> reference_code = $codeGen -> getToken();
		$purchase_code -> user_id = $user -> id;
		$purchase_code -> total_amount = $totalAmount;
		$purchase_code -> payment_method = $payment_method;
		$purchase_code -> quantity = $quantity;
		$purchase_code -> code_amount = $amount;
		if (strlen($senderBitcoinAddress) > 0 && $payment_method == 4) {
			$purchase_code -> company_wallet_address= $company_wallet_address;
			$purchase_code -> sender_wallet_address = $senderBitcoinAddress;
			$purchase_code -> btc_received_amount = $btc_amount;
		}
		$purchase_code -> remarks = $remarks;
		$purchase_code -> save();
		
		$payment_proofs_length = count($payment_proofs["name"]);
		if ($payment_method != 1) {
			for ($i = 0; $i < $payment_proofs_length; $i++) {
				$filename = $payment_proofs["name"][$i];
				$tmpName = $payment_proofs["tmp_name"][$i];
				$type = $payment_proofs["type"][$i];
				
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$allowed = array('png', 'PNG', 'jpg', 'JPG');
				if (!in_array($ext, $allowed)) {
					App::flash('message_status', false);
					App::flash('message', 'Invalid File Type');
					Response::redirect($this -> siteUrl('member/purchase/activation'));
					return;
				}
				
				$payment_proof_dir = "assets/images/purchase_proof/" . $user -> id;
				if (!file_exists($payment_proof_dir)) {
					mkdir($payment_proof_dir, 0777, TRUE);
				}
				
				$path = sprintf("%s/%s.%s", $payment_proof_dir, md5($filename), $ext);
				
				if (move_uploaded_file($tmpName, $path)) {
					$purchaseCodeProof = new PurchaseActivationProof();
					$purchaseCodeProof -> purchase_activation_id= $purchase_code -> id;
					$purchaseCodeProof -> file_name = $path;
					$purchaseCodeProof -> save();
				}
			}
		}
		
		$user -> is_for_activation = 1;
		$user -> save();

		App::flash('message_status', true);
		App::flash('message', 'Request successfully submitted.');
		Response::redirect($this -> siteUrl($redirect));
	}

}
