<?php

namespace IncomeHistory\Controllers;

use \User;
use \ExitHistory;
use \DirectReferrals;

use \App;
use \Sentry;
use \Input;
use \Response;
use \Member\BaseController;

class IncomeHistoryController extends BaseController {
	public function __construct() {
		parent::__construct ();
		$this->data ['active_menu'] = 'income_history';
	}
	
	public function index() {
		$user = Sentry::getUser ();
		$this -> data["title"] = "Income History";
		
		$dr_bonuses = DirectReferrals::where("recruiter_id","=",$user -> id) -> get();
		$this -> data['dr_bonuses'] = $dr_bonuses;
		
		$exit_bonuses = ExitHistory::where("user_id","=",$user -> id) -> select("*","table as tbl") -> get();
		$this -> data['exit_bonuses'] = $exit_bonuses;
		
		App::render ( '@incomehistory/index.twig', $this->data );
	}
	
}
