<?php

namespace IncomeHistory;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'IncomeHistory';
	}

	public function getModuleAccessor() {
		return 'incomehistory';
	}

	public function registerPublicRoute() {
		Route::resource('/income_history', 'IncomeHistory\Controllers\IncomeHistoryController');
	}

}
