<?php

namespace AdminActivation;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminActivation';
	}

	public function getModuleAccessor() {
		return 'adminactivation';
	}

	public function registerAdminRoute() {
		Route::resource('/activation', 'AdminActivation\Controllers\AdminActivationController');
		
		Route::get('/activation/:user_id/proofs', 'AdminActivation\Controllers\AdminActivationController:viewPaymentProof') -> name("activation_proof_payment");
		Route::post('/activation/account/activate', 'AdminActivation\Controllers\AdminActivationController:activateAccount');
		Route::post('/activation/paginated/list', 'AdminActivation\Controllers\AdminActivationController:paginatedList') -> name("admin_activation_paginated_list");
	}

}
