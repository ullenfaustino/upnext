<?php

namespace AdminActivation\Controllers;

use \App;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Response;
use \Admin\BaseController;

use \PurchaseActivationProof;
use \PurchaseActivation;
use \CodeGenerator;
use \Codes;
use \Vouchers;
use \Subscription;

class AdminActivationController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'account_activation';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Administrator Account Activation';
		
		App::render('@adminactivation/index.twig', $this -> data);
	}
	
	public function viewPaymentProof($user_id) {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Proof Documents';
		
		$proofs = PurchaseActivationProof::leftJoin("purchase_activation","purchase_activation.id","=","purchase_activation_proof.purchase_activation_id")
										-> where("purchase_activation.user_id", "=", $user_id) 
										-> get();
		$this -> data['purchase_proofs'] = $proofs;
		
		App::render('@adminactivation/purchase_proofs.twig', $this -> data);
	}
	
	public function activateAccount() {
		$curr_user = Sentry::getUser();
		
		$user_id = Input::post("user_id");
		$purchase_ref_code = Input::post("purchase_ref_code");
		
		$user = Users::find($user_id);
		$purchase_activation = PurchaseActivation::where("reference_code", "=", $purchase_ref_code) -> first();
		
		$user -> is_for_activation = 2;
		
		if ($user -> package_type == 1) {
			$expiration = new \DateTime('now');
			$expiration-> modify('+1 year');
			$expiration_date = $expiration-> format('Y-m-d H:i:s');
			
			$subscription = new Subscription();
			$subscription -> user_id = $user -> id;
			$subscription -> expiration_date = $expiration_date;
			$subscription -> is_active = 1;
			$subscription -> save();
		}
		
		if ($user -> package_type == 2) {
			$voucher = new Vouchers();
			$voucher -> ref_code = "UPNXT1-" . $purchase_activation -> reference_code;
			$voucher -> user_id = $user -> id;
			$voucher -> voucher_amount = 750;
			$voucher -> is_voucher = 0;
			$voucher -> save();
			
			$params["reference_code"] = $user -> ref_code;
			$params["transaction_code"] = $voucher -> ref_code;
			$params["account_code"] = $user -> username;
			$params["account_name"] = $user -> full_name;
			$params["amount"] = $voucher -> voucher_amount;
			$params["date_issued"] = date("F j, Y", strtotime ( $voucher->created_at ));
			$params["email"] = $user -> email;
			$this -> generateOnTemplate($params, "stub");
		}
		
		if ($user -> package_type == 3) {
			$user -> is_registered = 1;
			
			$expiration = new \DateTime('now');
			$expiration-> modify('+6 months');
			$expiration_date = $expiration-> format('Y-m-d H:i:s');
			
			$subscription = new Subscription();
			$subscription -> user_id = $user -> id;
			$subscription -> expiration_date = $expiration_date;
			$subscription -> is_active = 1;
			$subscription -> save();
		}
		
		$purchase_activation -> approved_by = $curr_user -> id;
		$purchase_activation -> status = 1;
		
		$codeGenerator = new CodeGenerator();
		$token = $codeGenerator -> getCodes();
		
		$code = new Codes();
		$code -> generated_code = $token;
		$code -> purchase_activation_id= $purchase_activation-> id;
		
		if ($code -> save()) {
			$user -> save();
			$purchase_activation -> save();
			
			$to = $user -> email;
			$subject = "[UpNext - Online Shop] Account Activated";
			$body = "<p style='color: blue; font-weight: bold;'><b>Your payment has been successfully verified.</b><p>";
			$body .= "<p>";
			$body .= "<span>Hi </span>: <b>" . $user -> full_name . "</b>";
			$body .= "</p>";
			$body .= "<p>";
			$body .= "<span>Username: </span>: <b>" . $user -> username . "</b>";
			$body .= "</p>";
			$body .= "<p>";
			$body .= "<span>Your activation code is </span>: <b>" . $token . "</b>";
			$body .= "</p>";
			
			$mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
			pclose(popen($mail_cmd, "w"));
			
			App::flash("message_status", true);
			App::flash("message", "Account successfully activated.");
		} else {
			App::flash("message_status", false);
			App::flash("message", "Unable to Generate Code");
		}
		Response::redirect($this -> siteUrl("admin/activation"));
	}
	
	public function paginatedList() {
		$draw = Input::post ( "draw" );
		$limit = Input::post ( "length" );
		$start = Input::post ( "start" );
		$current_page = ceil ( ($start + 1) / 10 );
		$searchValue = trim ( Input::post ( "search" ) ['value'], '"' );
		$order = Input::post ( "order" ) [0] ['column'];
		$dir = Input::post ( "order" ) [0] ['dir'];
		$hasSearchKeyword = strlen ( $searchValue ) > 0;
		
		$select = array (
			"*", "users.id as user_id"
		);
		
		$accounts = Users::leftJoin("purchase_activation","purchase_activation.user_id","=", "users.id")
						->select ( $select )
						-> where("purchase_activation.status","=",0)
						-> where("is_registered","=",0)
						-> where("is_for_activation","=",1);
		
		$accountsFiltered = Users::leftJoin("purchase_activation","purchase_activation.user_id","=", "users.id")
								-> select ( $select )
								-> where("purchase_activation.status","=",0)
								-> where("is_registered","=",0)
								-> where("is_for_activation","=",1)
								-> where ( function ($query) use ($searchValue) {
									return $query->where ( 'purchase_activation.reference_code', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.username', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.full_name', 'LIKE', '%' . $searchValue . '%' )
												->orWhere ( 'users.email', 'LIKE', '%' . $searchValue . '%' );
								});
			
		$totalCount = $accounts->count ();
		$filteredCount = $accountsFiltered->count ();
		switch ($order) {
			case '0' :
				$col = 'users.id';
				break;
			case '1' :
				$col = 'purchase_activation.reference_code';
				break;
			case '2' :
				$col = 'users.username';
				break;
			case '3' :
				$col = 'users.full_name';
				break;
			case '4' :
				$col = 'users.email';
				break;
			case '5' :
				$col = 'users.package_type';
				break;
			case '6' :
				$col = 'purchase_activation.total_amount';
				break;
			case '7' :
				$col = 'purchase_activation.payment_method';
				break;
			case '8' :
				$col = 'purchase_activation.sender_wallet_address';
				break;
			case '9' :
				$col = 'purchase_activation.btc_received_amount';
				break;
			case '10' :
				$col = 'users.created_at';
				break;
			default :
				$col = 'users.created_at';
		}
		
		$accountsData = $accountsFiltered ->orderby ( $col, $dir )->offset ( $start )->take ( $limit )->get ();
		
		$arrData = [];
		foreach ( $accountsData as $key => $value ) {
			$action = "";
			$action .= "<form method='post' action='activation/account/activate' id='form-activate-account-" . $value -> user_id . "'>";
			$action .= "<input type='hidden' name='purchase_ref_code' value=" . $value -> reference_code. ">";
			$action .= "<input type='hidden' name='user_id' value=" . $value -> user_id. ">";
			$action .= "<button type='submit' title='Activate Account' class=\"btn btn-default btn-icon waves-effect waves-circle waves-float\" data-row-id=" . $value -> user_id . ">";
			$action .= "<span class=\"zmdi zmdi-check\"></span></button></form>";
			$action .= "<a href='/admin/activation/" . $value -> user_id . "/proofs' target='_blank' title='Proof of Payment' class=\"btn btn-default btn-icon waves-effect waves-circle waves-float\" data-row-id=" . $value -> user_id . "><span class=\"zmdi zmdi-apps\"></span></a>";
			
			$arrData [] = [
					$value->user_id,
					$value->reference_code,
					$value->username,
					$value->full_name,
					$value->email,
					"Option " . $value->package_type,
					sprintf ( "<b>%s</b>", number_format ( $value->total_amount, 2, '.', ',' ) ),
					(($value->payment_method == 1) ? "CASH" : (($value->payment_method == 2) ? "Bank Deposit" : (($value->payment_method == 3) ? "Remittance" : "Bitcoin Payment"))),
					$value->sender_wallet_address,
					sprintf ( "<b>%s</b>", number_format ( $value->btc_received_amount, 8, '.', ',' ) ),
					sprintf("<span style='display: -webkit-inline-box;'>%s</span>", $action),
					date ( "F j, Y", strtotime ( $value->created_at ) ),
			];
		}
		
		$response ['draw'] = $draw;
		$response ['recordsTotal'] = $totalCount;
		$response ['recordsFiltered'] = $filteredCount;
		$response ['data'] = $arrData;
		$response ['zorder'] = $order;
		$response ['zdir'] = $dir;
		$response ['current_page'] = $current_page;
		$response ['search'] = $searchValue;
		$response ['raw_data'] = $accountsData;
		
		Response::headers ()->set ( 'Content-Type', 'application/json' );
		Response::setBody ( json_encode ( $response ) );
	}
}
