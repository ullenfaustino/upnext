<?php

namespace AdminDashboard\Controllers;

use \App;
use \View;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;

use \CompanyAllocation;

class AdminDashboardController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_dashboard';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Administrator Dashboard';
		

		App::render('@admindashboard/index.twig', $this -> data);
	}
	
	public function pagingatedAllocations() {
		$draw = Input::post ( "draw" );
		$limit = Input::post ( "length" );
		$start = Input::post ( "start" );
		$current_page = ceil ( ($start + 1) / 10 );
		$searchValue = trim ( Input::post ( "search" ) ['value'], '"' );
		$order = Input::post ( "order" ) [0] ['column'];
		$dir = Input::post ( "order" ) [0] ['dir'];
		$hasSearchKeyword = strlen ( $searchValue ) > 0;
		
		$select = array (
				"*"
		);
		
		$allocations = CompanyAllocation::select ( $select )
										-> leftJoin("users","users.id","=","company_allocation.user_id");
		
		$allocationsFiltered = CompanyAllocation::select ( $select )
												-> leftJoin("users","users.id","=","company_allocation.user_id")
												-> where ( function ($query) use ($searchValue) {
													return $query->where ( 'users.ref_code', 'LIKE', '%' . $searchValue . '%' )
													->orWhere ( 'users.username', 'LIKE', '%' . $searchValue . '%' )
													->orWhere ( 'users.full_name', 'LIKE', '%' . $searchValue . '%' );
												});
			
		$totalCount = $allocations->count ();
		$filteredCount = $allocationsFiltered->count ();
		
		// object summaries
		$alloc_sum['product_cost'] = number_format($allocations -> sum("product_cost") , 2, '.', ',');
		$alloc_sum['operating_exp'] = number_format($allocations -> sum("operating_exp"), 2, '.', ',');
		$alloc_sum['incentives'] = number_format($allocations -> sum("incentives"), 2, '.', ',');
		$alloc_sum['misc'] = number_format($allocations -> sum("misc"), 2, '.', ',');
		$alloc_sum['direct'] = number_format($allocations -> sum("direct"), 2, '.', ',');
		$alloc_sum['matrix'] = number_format($allocations -> sum("matrix"), 2, '.', ',');
		$alloc_sum['company_a_profit'] = number_format($allocations -> sum("company_a_profit"), 2, '.', ',');
		$alloc_sum['company_b_profit'] = number_format($allocations -> sum("company_b_profit"), 2, '.', ',');
		
		$total_alloc_sum = $allocations -> sum("product_cost") 
							+ $allocations -> sum("operating_exp") 
							+ $allocations -> sum("incentives") 
							+ $allocations -> sum("misc")
							+ $allocations -> sum("direct")
							+ $allocations -> sum("matrix")
							+ $allocations -> sum("company_a_profit")
							+ $allocations -> sum("company_b_profit");
		$alloc_sum['total_company_income'] = number_format($total_alloc_sum, 2, '.', ',');
		
		switch ($order) {
			case '0' :
				$col = 'users.full_name';
				break;
			default :
				$col = 'company_allocation.created_at';
		}
		
		$allocationsData = $allocationsFiltered-> orderby($col, $dir) -> offset($start) -> take($limit) -> get();
		
		$arrData = [];
		foreach ( $allocationsData as $key => $value ) {
			$arrData [] = [
				$value -> full_name,
				$value -> username,
				number_format ( $value -> product_cost, 2, '.', ',' ),
				number_format ( $value -> operating_exp, 2, '.', ',' ),
				number_format ( $value -> incentives, 2, '.', ',' ),
				number_format ( $value -> misc, 2, '.', ',' ),
				number_format ( $value -> direct, 2, '.', ',' ),
				number_format ( $value -> matrix, 2, '.', ',' ),
				number_format ( $value -> company_a_profit, 2, '.', ',' ),
				number_format ( $value -> company_b_profit, 2, '.', ',' ),
				date ("F j, Y h:i A", strtotime($value -> created_at )),
			];
		}
		
		$response ['draw'] = $draw;
		$response ['recordsTotal'] = $totalCount;
		$response ['recordsFiltered'] = $filteredCount;
		$response ['data'] = $arrData;
		$response ['zorder'] = $order;
		$response ['zdir'] = $dir;
		$response ['current_page'] = $current_page;
		$response ['search'] = $searchValue;
		$response ['raw_data'] = $allocationsData;
		$response ['allocation_summary'] = $alloc_sum;
		
		Response::headers ()->set ( 'Content-Type', 'application/json' );
		Response::setBody ( json_encode ( $response ) );
	}

}
