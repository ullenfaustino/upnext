<?php

namespace AdminDashboard;

use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminDashboard';
	}

	public function getModuleAccessor() {
		return 'admindashboard';
	}

	public function registerAdminRoute() {
		Route::resource('/dashboard', 'AdminDashboard\Controllers\AdminDashboardController');
		
		Route::post('/dashboard/paginated/allocations', 'AdminDashboard\Controllers\AdminDashboardController:pagingatedAllocations') -> name("paginated_allocations");
	}

}
