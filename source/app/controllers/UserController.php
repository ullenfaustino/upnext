<?php

class UserController extends BaseController {

	public function index() {
	}

	public function landingPage() {
		$this -> data['title'] = 'UpNext - Online Shop';
		
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> hasAnyAccess(array('admin'))) {
				Response::redirect($this -> siteUrl('admin'));
			} elseif ($user -> hasAnyAccess(array('member'))) {
				Response::redirect($this -> siteUrl('member'));
			} else {
				Response::redirect($this -> siteUrl('non-member'));
			}
		} else {
			View::display('landing.twig', $this -> data);
		}
	}

	public function registration() {
		Response::redirect($this -> siteUrl("/"));
	}

	/**
	 * display the login form
	 */
	public function login() {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> hasAnyAccess(array('admin'))) {
				Response::redirect($this -> siteUrl('admin'));
			} elseif ($user -> hasAnyAccess(array('member'))) {
				Response::redirect($this -> siteUrl('member'));
			} else {
				Response::redirect($this -> siteUrl('non-member'));
			}
		} else {
			Response::redirect($this -> siteUrl("/"));
		}
	}
	
	public function userProfile_view($user_id) {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> user_type == 2 && $user -> id != $user_id) {
				Response::redirect($this -> siteUrl('/login'));
				return;
			}
			$this -> data['user'] = $user;
			$this -> data['title'] = "Profile";
			
			$direct_referrals = DirectReferrals::leftJoin("users as User","User.id","=","direct_referrals.recruitee_id")
											-> where("direct_referrals.recruiter_id","=",$user -> id)
											-> select(array("*"))
											-> get();
			$this -> data['direct_referrals'] = $direct_referrals;
			
			$this -> data['can_change_password'] = ($user -> user_type == 2 && $user -> id == $user_id) ? true : false;
			
			View::display('user/user_profile.twig', $this -> data);
		} else {
			Response::redirect($this -> siteUrl('/login'));
		}
	}

	/**
	 * Account Activation View
	 */
	public function accountActivationView($user_id, $activation_code) {
		$user = Sentry::findUserById($user_id);
		if ($user -> isActivated()) {
			Response::redirect($this -> siteUrl('/login'));
			return;
		}
		$this -> data['title'] = '[UpNext - Trading] Account Activation';
		$user = User::find($user_id);
		$this -> data['user'] = $user;
		$this -> data['user_id'] = $user_id;
		$this -> data['account_activation_code'] = $activation_code;
		View::display('user/account_activation.twig', $this -> data);
	}

	public function activateAccount() {
		$user_id = Input::post("user_id");
		$account_activation_code = Input::post('account_activation_code');
		$matrix_count= Input::post("matrix_count");
		$birthday = Input::post("birthday");
		$address = Input::post("address");
		$contact_number = Input::post("contact_number");
		try {
			$user = Sentry::findUserById($user_id);
			$user -> matrix_count= $matrix_count;
			$birthday = strtr($birthday, '/', '-');
			$user -> birthday = date("Y-m-d", strtotime($birthday));
			$user -> address = $address;
			$user -> contact_no = $contact_number;

			if ($user -> attemptActivation($account_activation_code)) {
                $user -> save();
				Sentry::login($user, false);
			} else {
				App::flash('message_status', false);
				App::flash('message', 'Failed to activate User account.');
			}
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			App::flash('message_status', false);
			App::flash('message', $e -> getMessage());
			
			Response::redirect($this -> siteUrl(sprintf('/registration/confirmation/%s/%s', $user_id, $account_activation_code)));
            return;
		} catch (\Exception $e) {
		    App::flash('message_status', false);
            App::flash('message', $e -> getMessage());
            
            Response::redirect($this -> siteUrl(sprintf('/registration/confirmation/%s/%s', $user_id, $account_activation_code)));
            return;
		}
		Response::redirect($this -> siteUrl('/login'));
	}

	/**
	 * Process the login
	 */
	public function doLogin() {
		$username = Input::post('username');
		$password = Input::post('password');
		try {
			$credential = array('username' => $username, 'password' => $password, 'activated' => 1);

			// Try to authenticate the user
			$user = Sentry::authenticate($credential, false);
			Sentry::login($user, false);
		} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$user = Sentry::findUserByLogin($username);
			$master = MasterPassword::where('is_active', '=', 1) -> get();
			// Check the password
			if (md5(Input::post('password')) === $master[0] -> password) {
				try {
					Sentry::login($user, false);
				} catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e ) {
					App::flash('message_status', false);
					App::flash('message', $e -> getMessage());
				}
			} else {
				App::flash('message_status', false);
				App::flash('message', $e -> getMessage());
				App::flash('username', $username);
			}
		} catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e ) {
			App::flash('message_status', false);
			App::flash('message', $e -> getMessage());
		} catch(\Exception $e) {
			App::flash('message_status', false);
			App::flash('message', $e -> getMessage());
			App::flash('username', $username);
		}
		Response::redirect($this -> siteUrl("/"));
	}

	/**
	 * Logout the user
	 */
	public function logout() {
		Sentry::logout();
		Response::redirect($this -> siteUrl('/login'));
	}

	/**
	 * Pre-Registration
	 */
	public function preRegistration() {
		$codeGen = new CodeGenerator();
        
        //$username = Input::post("username");
		$username = "UN" . $codeGen -> getUsername(5);
        $full_name = Input::post("full_name");
        $email = Input::post("email");
        $package_type = Input::post("package_type");
        
        try {
//         	$email_exists = Users::where("email","=",$email) -> first();
//         	if ($email_exists) {
//         		App::flash('message', "Email already exists.");
//         		App::flash('message_status', false);
//         		Response::redirect($this -> siteUrl('/'));
//         		return;
//         	}

	        $randomPassword = $codeGen -> getToken(12);
	        $pin_code = $codeGen -> getToken(4);
	        $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
	                        'username' => trim($username), 
	                        'email' => trim($email), 
	                        'full_name' => trim($full_name), 
	                        'password' => $randomPassword, 
	        				'pin_code' => $pin_code, 
	        				'package_type' => $package_type, 
	                        'canonical_hash' => base64_encode($randomPassword),
	        				'activated' =>  ($package_type == 3) ? 1 : 0, 
	        				'permissions' => array('member' => 1),
	                        'user_type' => 2
	                       ];
	        
	        $user = Sentry::createUser($credential);
	        $user -> addGroup(Sentry::getGroupProvider() -> findByName("Members"));
	        $activationCode = $user -> getActivationCode();
	        
	        $sendVerification = GenericHelper::sendAccountVerification($user, $activationCode, $randomPassword);
	        if ($sendVerification) {
	            echo "completed.";
	            
	            App::flash('message', "Registration Completed, Your account credentials has been sent to your email.");
				App::flash('message_status', true);
	        } else {
	           	App::flash('message', "Unable to send email verification.");
				App::flash('message_status', false);
	        }
        } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
			App::flash('message_status', false);
			App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
		} catch ( \Exception $e ) {
			App::flash('message_status', false);
			App::flash('message', "[Exception:]" . $e -> getMessage());
		}
		Response::redirect($this -> siteUrl('/'));
	}

	/**
	 * Re-Send Verification Code via email
	 */
	public function resendVerificationCode($user_id) {
		$user = Users::find($user_id);
		if ($user) {
			if ($user -> activated == 0) {
				GenericHelper::resendEmailVerification($user);
			} else {
				echo "This account is already activated.";
			}
		} else {
			echo "User Not Found.";
		}
	}
	
	/*
	* updates for user profile
	*/
	
	public function updateBasicInfo() {
		try {
			$user = Sentry::getUser();
			$birthday = Input::post("birthday");
			$full_name = Input::post("full_name");
			
			$birthday = strtr($birthday, '/', '-');
			$user -> birthday = date("Y-m-d", strtotime($birthday));
			$user -> full_name = $full_name;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateBankDetails() {
		try {
			$user = Sentry::getUser();
			$bank_name = Input::post("bank_name");
			$account_name = Input::post("account_name");
			$account_number = Input::post("account_number");
			
			$user -> account_name = $account_name;
			$user -> account_number = $account_number;
			$user -> bank_name = $bank_name;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateContactDetails() {
		try {
			$user = Sentry::getUser();
			$contact_no = Input::post("contact_no");
			$email = Input::post("email");
			
			$user -> contact_no = $contact_no;
			$user -> email = $email;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updatePassword() {
		try {
			$user = Sentry::getUser();
			$current_password = Input::post("current_password");
			$new_password = Input::post("new_password");
			$confirm_password = Input::post("confirm_password");
			
			if ($new_password !== $confirm_password) {
				App::flash("message_status", false);
				App::flash('message', 'Password does not match!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			if ($user -> checkPassword($current_password)) {
				$user -> password = $confirm_password;
				$user -> canonical_hash = base64_encode($confirm_password);
				$user -> save();
				
				//TODO: send email notification here

				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', 'Invalid Current Password!');
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updatePinCode() {
		try {
			$user = Sentry::getUser();
			$current_pin_code = Input::post("current_pin_code");
			$new_pin_code = Input::post("new_pin_code");
			$confirm_pin_code = Input::post("confirm_pin_code");
			
			if ($new_pin_code !== $confirm_pin_code) {
				App::flash("message_status", false);
				App::flash('message', 'Pin code does not match!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			if ($user -> pin_code == $current_pin_code) {
				$user -> pin_code = $confirm_pin_code;
				$user -> save();
				
				//TODO: send email notification here

				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', 'Invalid Current Pin Code!');
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateAvatar() {
		try {
			$user = Sentry::getUser();
			
			$avatar = Input::file("upload_avatar");
			
			$filename = $avatar["name"];
			$tmpName = $avatar["tmp_name"];
			$type = $avatar["type"];
			
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed = array('png', 'PNG', 'jpg', 'JPG');
			if (!in_array($ext, $allowed)) {
				App::flash("message_status", false);
				App::flash('message', 'Invalid File Type!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			$avatar_dir = "assets/images/avatar/";
			if (!file_exists($avatar_dir)) {
				mkdir($avatar_dir, 0777, TRUE);
			}
			
			$filename = md5($filename);
			$path = sprintf("%s/%s.%s", $avatar_dir, $filename, $ext);
			
			if (move_uploaded_file($tmpName, $path)) {
				if ($user -> avatar !== "default-avatar.png") {
					if (file_exists($avatar_dir > $user -> avatar)) {
						unlink($avatar_dir > $user -> avatar);
					}
				}
				$user -> avatar = $filename . "." . $ext;
				$user -> save();
				
				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', "Unable to update avatar!");
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			App::flash("message_status", false);
            App::flash('message', $e -> getMessage());
			Response::redirect($this -> siteUrl("/login"));
		}
	}
}
