<?php

namespace Member;

use \Sentry;
use \Response;

class MemberController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		
		if ($user -> package_type == 3) {
			if ($user -> is_registered == 1) {
				echo "this site is coming soon.";
			} else {
				if ($user -> is_for_activation == 1) {
					Response::Redirect($this -> siteUrl("member/purchase/waiting/activation"));
				} else {
					Response::Redirect($this -> siteUrl("member/purchase/activation"));
				}
			}
		} else {
			if ($user -> is_registered == 1) {
				Response::Redirect($this -> siteUrl(sprintf('member/geneology/%s', $user -> ref_code)));
			} else {
				if ($user -> is_for_activation == 1) {
					Response::Redirect($this -> siteUrl("member/purchase/waiting/activation"));
				} else if($user -> is_for_activation == 2) {
					Response::Redirect($this -> siteUrl("member/register/geneology"));
				} else {
					Response::Redirect($this -> siteUrl("member/purchase/activation"));
				}
			}
		}
	}

}
