<?php

class APIController extends BaseController {

	public function showDreamGirl() {
		header("Content-type: image/jpeg");
		readfile("assets/images/special/dream.JPG");
		exit(0);
	}

	public function lucky_lotto_winning_number($series_count, $max_digit) {
		$this -> getLuckyNumber($series_count, $max_digit);
	}
	
	private function getLuckyNumber($series_count, $max_digit) {
		$arrayWinningNumbers = array();
		$lucky_num = rand(1, $max_digit);
		
		if (in_array($lucky_num, $arrayWinningNumbers)) {
			$lucky_num = rand(1, $max_digit);
		} else {
			array_push($arrayWinningNumbers, $lucky_num);
		}
		
		echo $lucky_num . " ";
		if ($series_count == 1) { // end of recursion
			return;
		} else {
			return $this -> getLuckyNumber(($series_count-1), $max_digit);
		}
	}

	public function generateCodes($count) {
		Response::headers() -> set('Content-Type', 'application/json');

		$codeGenerator = new CodeGenerator();

		$codeTransaction = new CodesTransaction();
		$codeTransaction -> generated_by_id = 1;
		// $codeTransaction -> issued_to_id = $recipientUser -> id;
		$codeTransaction -> issued_to_id = 1;

		if ($codeTransaction -> save()) {
			for ($i = 0; $i < $count; $i++) {
				$token = $codeGenerator -> getCodes();

				$code = new Codes();
				$code -> generated_code = $token;

				if ($code -> save()) {
					$transactionCodes = new CodesTransactionHasCodes();
					$transactionCodes -> codes_transaction_id = $codeTransaction -> id;
					$transactionCodes -> codes_id = $code -> id;
					$transactionCodes -> save(array("timestamps" => false));
				}
			}
		}

		Response::setBody(json_encode("success"));
	}

	public function getGeneology($id, $table) {
		Response::headers() -> set('Content-Type', 'application/json');

		$siblings = UsersHasHierarchySiblings::siblings($id, $table) -> get() -> toArray();
		$data['parent'] = Sentry::findUserById($id);
		// load 1st gen child
		foreach ($siblings as $key => $child) {
			if ($child['position'] == 0) {
				$data['child_left'] = Sentry::findUserById($child['recruitee_id']);
				$siblings0 = UsersHasHierarchySiblings::siblings($child['recruitee_id'], $table) -> get() -> toArray();

				// 2nd gen
				foreach ($siblings0 as $key => $child0) {
					if ($child0['position'] == 0) {
						$data['child_left00'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings000 = UsersHasHierarchySiblings::siblings($child0['recruitee_id'], $table) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings000 as $key => $child00) {
							if ($child00['position'] == 0) {
								$data['child_left000'] = Sentry::findUserById($child00['recruitee_id']);
							} else if ($child00['position'] == 1) {
								$data['child_left001'] = Sentry::findUserById($child00['recruitee_id']);
							}
						}
					} else if ($child0['position'] == 1) {
						$data['child_left01'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings001 = UsersHasHierarchySiblings::siblings($child0['recruitee_id'], $table) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings001 as $key => $child01) {
							if ($child01['position'] == 0) {
								$data['child_left010'] = Sentry::findUserById($child01['recruitee_id']);
							} else if ($child01['position'] == 1) {
								$data['child_left011'] = Sentry::findUserById($child01['recruitee_id']);
							}
						}
					}
				}
			} else if ($child['position'] == 1) {
				$data['child_right'] = Sentry::findUserById($child['recruitee_id']);
				$siblings0 = UsersHasHierarchySiblings::siblings($child['recruitee_id'], $table) -> get() -> toArray();

				// 2nd gen
				foreach ($siblings0 as $key => $child0) {
					if ($child0['position'] == 0) {
						$data['child_right00'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings000 = UsersHasHierarchySiblings::siblings($child0['recruitee_id'], $table) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings000 as $key => $child00) {
							if ($child00['position'] == 0) {
								$data['child_right000'] = Sentry::findUserById($child00['recruitee_id']);
							} else if ($child00['position'] == 1) {
								$data['child_right001'] = Sentry::findUserById($child00['recruitee_id']);
							}
						}
					} else if ($child0['position'] == 1) {
						$data['child_right01'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings001 = UsersHasHierarchySiblings::siblings($child0['recruitee_id'], $table) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings001 as $key => $child01) {
							if ($child01['position'] == 0) {
								$data['child_right010'] = Sentry::findUserById($child01['recruitee_id']);
							} else if ($child01['position'] == 1) {
								$data['child_right011'] = Sentry::findUserById($child01['recruitee_id']);
							}
						}
					}
				}
			}
		}

		$hierarchy = BonusManager::getHierarchyCount($id, $table);
		$data['heirarchy'] = $hierarchy;
		Response::setBody(json_encode($data));
	}

	public function getUserDetail($user_id, $table) {
		Response::headers() -> set('Content-Type', 'application/json');
		
		$curr_user = Sentry::getUser();

		$user = Users::find($user_id);
		$color = "geneology/upnexIconAvailable.png";
		if ($user) {
			$color = sprintf("geneology/upnexIconGraduate%s.png", $table);
		}
		$response['color'] = $color;

		$direct = DirectReferrals::getDirect($user_id) -> first();
		$response['dr'] = null;
		if ($direct) {
			$response['dr'] = $direct;
		}

		Response::setBody(json_encode($response));
	}

	public function validateDR($username) {
		Response::headers() -> set('Content-Type', 'application/json');

		$user = Users::where("username", "=", $username) -> first();
		if ($user) {
			$response['status'] = "success";
			$response['user'] = $user;
		} else {
			$response['status'] = "error";
		}

		Response::setBody(json_encode($response));
	}

	public function webhookHandler($wallet_address) {
		try {
			$codeGen = new CodeGenerator();
			$payload = BlocktrailSDK::getWebhookPayload();
			if ($payload['event_type'] === "address-transactions") {
				$hashCode = $payload['data']['hash'];
				$bitcoin_amount = $payload['addresses'][$wallet_address];
				$user = Users::where('company_associated_wallet', '=', $wallet_address) -> first();
				if ($user && ($bitcoin_amount > 0)) {

					// $subject = "[Argon Coin] Investment Received from " . $wallet_address . " , trx. code: " . $hashCode.
					// $contentBody = "<p>You have successfully invested " . GenericHelper::decodeSatoshi($bitcoin_amount) . " BTC.</p>";
					// $this -> sendEmailNotificationReceivedIntoWallet($subject, $contentBody, $user -> email);
				}
			}
		} catch(\Exception $e) {
			echo $e -> getMessage();
			$subject = "Error on webhook handler on blocktrail";
			$contentBody = "<p>Error message : " . $e -> getMessage() . "</p>";
			$this -> sendEmailNotificationReceivedIntoWallet($subject, $contentBody);
		}
	}

}
