<?php
use Illuminate\Database\Eloquent\Model as Model;

class DirectReferrals extends Model {
    protected $table = "direct_referrals";
    
    public function countDirect($id) {
        $direct = DirectReferrals::where('recruiter_id','=',$id)->get()->toArray();
        return count($direct);
    }
    
    public function getDirect($id) {
        return DirectReferrals::join('users as u','u.id','=','direct_referrals.recruiter_id')
                    ->where('direct_referrals.recruitee_id', '=', $id)
                    ->select(['*', 'u.id as uid','u.username']);
    }
    
    public function hasRegistered($id) {
        $DR = DirectReferrals::where('recruitee_id', '=', $id) -> get() -> toArray();
        if (count($DR) === 0) {
            return false;
        } else {
            return true;
        }
    }
}
