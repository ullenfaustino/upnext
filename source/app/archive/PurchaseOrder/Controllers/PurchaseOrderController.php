<?php

namespace PurchaseOrder\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \OnlinePurchases;
use \BankList;
use \CompanyBankAccounts;

class PurchaseOrderController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('member_sidebar') -> setActiveMenu('purchaseorder');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Code';
		$this -> data['users'] = User::groupByCreatedAt($user -> id) -> get() -> toArray();
		$requests = OnlinePurchases::getPurchases($user -> id) -> get() -> toArray();
		;
		$this -> data['requests'] = $requests;

		$this -> data['user'] = $user;

		$this -> data['banks'] = CompanyBankAccounts::all();
		$this -> data['remits'] = Banklist::getRemittances();

		/** set secret token **/
		$secret = md5(uniqid(rand(), true));
		$this -> data['form_secret'] = $secret;
		$_SESSION['FORM_SECRET'] = $secret;

		/** load the user.js app */
		$this -> loadJs('app/user.js');

		/** publish necessary js  variable */
		$this -> publish('baseUrl', $this -> data['baseUrl']);

		/** render the template */
		View::display('@purchaseorder/index.twig', $this -> data);
	}

	public function doOrderRequest() {
		$form_secret = Input::post('form_secret') != null ? Input::post('form_secret') : '';
		if (isset($_SESSION["FORM_SECRET"])) {
			if (strcasecmp($form_secret, $_SESSION["FORM_SECRET"]) === 0) {
				$redirect = Request::getResourceUri();

				$user = Sentry::getUser();
				$gateway = Input::post('gateway');
				$sender = Input::post('sender');
				$amount = Input::post('amount');
				$codeCount = Input::post('code_count');
				$refNumber = Input::post('reference_no');
				$dateSent = date("Y-m-d", strtotime(Input::post('date_sent')));
				$timeSent = Input::post('time_sent');
				$option = Input::post('opt_payment');

				$request = new OnlinePurchases();
				$request -> user_id = $user -> id;
				$request -> date_sent = $dateSent;
				$request -> time_sent = $timeSent;
				$request -> status_id = 0;
				$request -> no_of_codes = $codeCount;
				$request -> sender_name = $sender;
				$request -> paid_amount = str_replace(",", "", $amount);
				if ($option == 0) {
					$request -> gateway = "Cash";
				} else {
					$request -> reference_no = $refNumber;
					if ($gateway === "0" || $gateway === "1") {
						$request -> gateway = Input::post('other_gateway');
					} else {
						$request -> gateway = $gateway;
					}

					$file = Input::file('img_upload');
					$tempName = $file["tmp_name"];
					$filename = $file["name"];

					$allowed = ' ,png,jpg,JPG,PNG';
					$extension_allowed = explode(',', $allowed);

					$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
					if (!array_search($file_extension, $extension_allowed)) {
						App::flash('message', "Invalid Image File Type!" . $filename);
						App::flash('message_status', false);
						Response::redirect($this -> siteUrl('/') . 'member/purchaseorder?redirect=' . base64_encode($redirect));
						unset($_SESSION["FORM_SECRET"]);
						return;
					}

					$type = pathinfo($filename, PATHINFO_EXTENSION);
					$img = 'assets/reciept/' . $filename;

					if (move_uploaded_file($tempName, $img)) {
						$request -> img_path = $img;
					}
				}

				if ($request -> save()) {
					App::flash('message', "Request successfully sent!");
					App::flash('message_status', true);
					Response::redirect($this -> siteUrl('/') . 'member/purchaseorder?redirect=' . base64_encode($redirect));
				} else {
					App::flash('message', "Server error!");
					Response::redirect($this -> siteUrl('/') . 'member/purchaseorder?redirect=' . base64_encode($redirect));
				}
				unset($_SESSION["FORM_SECRET"]);
			} else {
				App::flash('message_status', false);
				App::flash('message', "Invalid Token!");
				Response::redirect($this -> siteUrl('member/usercodes'));
			}
		}
	}

}
