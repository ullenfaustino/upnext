<?php

namespace UserActivation\Controllers;

use \User;
use \Users;
use \MembersProfiles;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \DirectReferrals;
use \LinkedUsers;
use \Codes;
use \UserBalance;
use \Constants;
use \ExitHistory;
use \OverrideHistory;
use \CodeGenerator;
use \ReEntryCodes;
use \CrosslineValidator;
use \BonusManager;

use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class UserActivationController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('member_sidebar') -> setActiveMenu('2useractivation');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$MF = MembersProfiles::where('user_id', '=', $user -> id) -> first();

		$this -> data['title'] = 'Account Activation';
		$this -> data['user'] = $user;
		$this -> data['member_id'] = $MF -> id;

		/** set secret token **/
		$secret = md5(uniqid(rand(), true));
		$this -> data['form_secret'] = $secret;
		$_SESSION['FORM_SECRET'] = $secret;

		/** load the user.js app */
		$this -> loadJs('app/user.js');

		/** publish necessary js  variable */
		$this -> publish('baseUrl', $this -> data['baseUrl']);

		/** render the template */
		View::display('@useractivation/index.twig', $this -> data);
	}

	public function accountActivationRegister() {
		$form_secret = Input::post('form_secret') != null ? Input::post('form_secret') : '';

		if (isset($_SESSION["FORM_SECRET"])) {
			if (strcasecmp($form_secret, $_SESSION["FORM_SECRET"]) === 0) {
				$user_id = Input::post('user_id');
				$member_profile_id = Input::post('member_profile_id');
				$sponsorUsername = Input::post('sponsor_username');
				$placement_head = Input::post('placement_head_username');
				$placement = Input::post('placement');
				$AccessCode = Input::post('activation_code');

				try {
					$_code = Codes::where('generated_code', '=', $AccessCode) -> get() -> toArray();
					$_code_id = $_code[0]['id'];
					$reEntry = ReEntryCodes::where('code_id', '=', $_code_id) -> get() -> toArray();
					if (count($reEntry) > 0) {
						App::flash('message_status', false);
						App::flash('message', "Invalid Activation Code!");

						$redirect = Request::getResourceUri();
						Response::redirect($this -> siteUrl('member/useractivation'));
						return;
					}
					if (count($_code) > 0) {
						if ($_code[0]['is_used'] === 0) {
							$_user = Sentry::getUser();
							$sponsor = Sentry::findUserByLogin($sponsorUsername);
							$head = Sentry::findUserByLogin($placement_head);

							if (!CrosslineValidator::isCrosslinedRegistration($sponsor -> id, $head -> id)) {
								App::flash('act_code', $AccessCode);
								App::flash('message_status', false);
								App::flash('message', "Crosslining is not permitted!");

								$redirect = Request::getResourceUri();
								Response::redirect($this -> siteUrl('member/useractivation'));
								return;
							}

							//validate same user itself is the sponsor
							if ($_user -> id === $sponsor -> id) {
								App::flash('act_code', $AccessCode);
								App::flash('message_status', false);
								App::flash('message', "Your cannot assign yourself as your sponsor!");

								$redirect = Request::getResourceUri();
								Response::redirect($this -> siteUrl('member/useractivation'));
								return;
							}

							//validate same user itself is the placement
							if ($_user -> id === $head -> id) {
								App::flash('act_code', $AccessCode);
								App::flash('message_status', false);
								App::flash('message', "Your cannot assign yourself as your placement head!");

								$redirect = Request::getResourceUri();
								Response::redirect($this -> siteUrl('member/useractivation'));
								return;
							}

							//validate if sponsor is already registered
							if (!DirectReferrals::hasRegistered($sponsor -> id)) {
								App::flash('act_code', $AccessCode);
								App::flash('message_status', false);
								App::flash('message', "Your Sponsor is not yet registered!");

								$redirect = Request::getResourceUri();
								Response::redirect($this -> siteUrl('member/useractivation'));
								return;
							}

							//validate if placementhead is already registered
							if (!DirectReferrals::hasRegistered($head -> id)) {
								App::flash('act_code', $AccessCode);
								App::flash('message_status', false);
								App::flash('message', "Your placement head is not yet registered!");

								$redirect = Request::getResourceUri();
								Response::redirect($this -> siteUrl('member/useractivation'));
								return;
							}

							//validate if placementhead has sibling in left or right
							$HEAD_LEFT = UsersHasHierarchySiblings::hasLeft($head -> id);
							$HEAD_RIGHT = UsersHasHierarchySiblings::hasRight($head -> id);

							if (strcasecmp($placement, "left") == 0) {
								if ($HEAD_LEFT) {
									App::flash('act_code', $AccessCode);
									App::flash('message_status', false);
									App::flash('message', "Placement Left position is not available!");

									$redirect = Request::getResourceUri();
									Response::redirect($this -> siteUrl('member/useractivation'));
									return;
								}
							} else {
								if ($HEAD_RIGHT) {
									App::flash('act_code', $AccessCode);
									App::flash('message_status', false);
									App::flash('message', "Placement Right position is not available!");

									$redirect = Request::getResourceUri();
									Response::redirect($this -> siteUrl('member/useractivation'));
									return;
								}
							}

							$hierarchy = new HierarchySiblings();
							$hierarchy -> recruitee_id = $user_id;

							if (strcasecmp($placement, "left") == 0) {
								$hierarchy -> position = 0;
							} else {
								$hierarchy -> position = 1;
							}

							if ($hierarchy -> save()) {
								$hasSiblings = new UsersHasHierarchySiblings();
								$hasSiblings -> user_id = $head -> id;
								$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
								$hasSiblings -> save();

								$direct = new DirectReferrals();
								$direct -> profile_id = $member_profile_id;
								$direct -> recruiter_id = $sponsor -> id;
								$direct -> recruitee_id = $user_id;
								$direct -> save();

								// $linkedUser = new LinkedUsers();
								// $linkedUser -> user_id = $user_id;
								// $linkedUser -> new_user_id = $user_id;
								// $linkedUser -> save();

								//update sponsor balance
								// $constants = Constants::where('name', '=', 'direct_referral_bonus') -> first();
								// $balToAdd = $constants -> value;

								// BonusManager::updateUserBalance($sponsor -> id, $balToAdd);
								BonusManager::processExitBonus(1, 4000);

								$TmpCode = Codes::find($_code[0]['id']);
								$TmpCode -> is_used = 1;
								$TmpCode -> user_id = $user_id;
								$TmpCode -> save();
							}
							$redirect = Request::getResourceUri();
							Response::redirect($this -> siteUrl('member'));
						} else {
							App::flash('act_code', $AccessCode);
							App::flash('message_status', false);
							App::flash('message', "Activation code is already in used!");

							$redirect = Request::getResourceUri();
							Response::redirect($this -> siteUrl('member/useractivation'));
							return;
						}
					} else {
						App::flash('act_code', $AccessCode);
						App::flash('message_status', false);
						App::flash('message', "Activation code is invalid!");

						$redirect = Request::getResourceUri();
						Response::redirect($this -> siteUrl('member/useractivation'));
						return;
					}
				} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
					App::flash('act_code', $AccessCode);
					App::flash('message_status', false);
					App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");

					$redirect = Request::getResourceUri();
					Response::redirect($this -> siteUrl('member/useractivation'));
				} catch(\Exception $e) {
					App::flash('act_code', $AccessCode);
					App::flash('message_status', false);
					App::flash('message', $e -> getMessage());

					$redirect = Request::getResourceUri();
					Response::redirect($this -> siteUrl('member/useractivation'));
				}
				unset($_SESSION["FORM_SECRET"]);
			} else {
				// Invald key secret
			}
		} else {
			Response::redirect($this -> siteUrl('member/useractivation'));
		}
	}

}
