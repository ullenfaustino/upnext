<?php

namespace EEXpressLoadCommission\Controllers;

use \AcsIncome;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Loader\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class XpressLoadCommissionController extends BaseController {
	public function __construct() {
		parent::__construct ();
		Menu::get ( 'loader_sidebar' )->setActiveMenu ( 'loadcommission' );
	}
	
	/**
	 * display list of resource
	 */
	public function index($page = 1) {
		$user = Sentry::getUser ();
		
		$this->data ['title'] = 'LOAD COMMISSION';
		$this->data ['user'] = $user;
		$this->data ['acs_income'] = AcsIncome::countTotalIncome ();
		
		/**
		 * load the user.js app
		 */
		$this->loadJs ( 'app/user.js' );
		
		/**
		 * publish necessary js variable
		 */
		$this->publish ( 'baseUrl', $this->data ['baseUrl'] );
		
		/**
		 * render the template
		 */
		View::display ( '@xpressloadcommission/index.twig', $this->data );
	}
}
