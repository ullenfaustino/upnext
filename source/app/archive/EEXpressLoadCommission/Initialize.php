<?php

namespace EEXpressLoadCommission;

use \MembersProfiles;
use \DirectReferrals;
use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'EEXpressLoadCommission';
	}

	public function getModuleAccessor() {
		return 'xpressloadcommission';
	}

	public function registerLoaderMenu() {
		$adminMenu = Menu::get('loader_sidebar');

		$member = $adminMenu -> createItem('loadcommission', array(
			'label' => 'LOAD COMMISSION', 
			'icon' => 'mobile-phone', 
			'url' => 'loader/commission'));

		$adminMenu -> addItem('loadcommission', $member);
	}

	public function registerLoaderRoute() {
		Route::resource('/commission', 'EEXpressLoadCommission\Controllers\XpressLoadCommissionController');
	}

}
