<?php

namespace GGAdminLoad\Controllers;

use \NetworkBalance;

use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \BankList;
use \LoadTransferTransaction;

class AdminLoadController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('admin_sidebar') -> setActiveMenu('adminload');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();

		$this -> data['title'] = 'Admin Power7 Booster Load';
		$this -> data['user'] = $user;

		$smart = NetworkBalance::where('id', '=', 4) -> first();
		$globe = NetworkBalance::where('id', '=', 5) -> first();
		$sun = NetworkBalance::where('id', '=', 6) -> first();

		$total = $smart -> balance + $globe -> balance + $sun -> balance;

		$this -> data['smart_balance'] = $smart -> balance;
		$this -> data['globe_balance'] = $globe -> balance;
		$this -> data['sun_balance'] = $sun -> balance;
		$this -> data['total_balance'] = $total;
		$this -> data['load_transfers'] = LoadTransferTransaction::getLoadTransferTransaction() -> get();

		/** load the user.js app */
		$this -> loadJs('app/user.js');

		/** publish necessary js  variable */
		$this -> publish('baseUrl', $this -> data['baseUrl']);

		/** render the template */
		View::display('@adminload/index.twig', $this -> data);
	}

}
