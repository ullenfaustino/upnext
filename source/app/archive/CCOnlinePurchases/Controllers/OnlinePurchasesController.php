<?php

namespace CCOnlinePurchases\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \OnlinePurchases;
use \Codes;
use \CodeGenerator;
use \CodesTransaction;
use \CodesTransactionHasCodes;

class OnlinePurchasesController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        Menu::get('admin_sidebar')->setActiveMenu('onlinepurchases');
    }

    /**
     * display list of resource
     */
    public function index()
    {
        $user = Sentry::getUser();
        $this->data['title'] = 'Online Purchases';
        $pendingRequests = OnlinePurchases::getPendingPurchases()
                                    ->get()
                                    ->toArray();
                                    
        $requests = OnlinePurchases::getActionPurchases()
                                    ->get()
                                    ->toArray();

        $cancelledRequests = OnlinePurchases::getCancelledPurchases()
                                    ->get()
                                    ->toArray();

        $this->data['pending_requests'] = $pendingRequests;
        $this->data['cancelled_requests'] = $cancelledRequests;
        $this->data['requests'] = $requests;
        $this -> data['user'] = $user;
       // var_dump($this->data['users']);

        /** load the user.js app */
        $this->loadJs('app/user.js');

        /** publish necessary js  variable */
        $this->publish('baseUrl', $this->data['baseUrl']);

        $secret = md5(uniqid(rand(), true));
        $this->data['form_secret'] = $secret;
        $_SESSION['ONLINE_PURCHASE_FORM_SECRET'] = $secret;

        /** render the template */
        View::display('@onlinepurchases/index.twig', $this->data);
    }

    public function reviewTransaction() {
        $redirect = Request::getResourceUri();

        $form_secret = Input::post('form_secret') != null ? Input::post('form_secret'):'';

        if(isset($_SESSION["ONLINE_PURCHASE_FORM_SECRET"])) {
            if(strcasecmp($form_secret, $_SESSION["ONLINE_PURCHASE_FORM_SECRET"]) === 0) {
                $transactionId = Input::post('transaction_id');
                $status = 0;

                if(Input::post('approved') != null) {
                    $status = 1;

                }
                else if(Input::post('cancelled') != null) {
                    $status = 2;
                }
                $admin = Sentry::getUser();
                $request = OnlinePurchases::findorFail($transactionId);
                var_dump($request -> status == 1);
                    $request -> status_id = $status;
                    $request -> no_of_codes;
                    $request -> admin_id = $admin -> id;
                    
                if($request -> save()) {
                    if($status == 1) {
                        $this -> generateCode( $request -> user_id,  $request -> no_of_codes);
                    }
                } 


                Response::redirect($this -> siteUrl('/') . 'admin/onlinepurchases?redirect=' . base64_encode($redirect));
                unset($_SESSION["ONLINE_PURCHASE_FORM_SECRET"]);
            }else {
                // Invald key secret
            }
        } else {
            Response::redirect($this -> siteUrl('/') . 'admin/onlinepurchases?redirect=' . base64_encode($redirect));
        }

       
    }

    private function generateCode($recipientId, $noOfCodes) {
        $redirect = Request::getResourceUri();

        if(Sentry::check()) {

            try {
                $codeGenerator = new CodeGenerator();

                $issuer = Sentry::getUser();


                $codeTransaction = new CodesTransaction();
                $codeTransaction -> generated_by_id = $issuer -> id;
                $codeTransaction -> issued_to_id = $recipientId;

                if($codeTransaction -> save()) {
                    for($i=0;$i<$noOfCodes;$i++) {
                        $token = $codeGenerator -> getToken(16);

                        $code = new Codes();
                        $code -> generated_code = $token;

                        if($code -> save()) {
                            $transactionCodes = new CodesTransactionHasCodes();
                            $transactionCodes -> codes_transaction_id = $codeTransaction -> id;
                            $transactionCodes -> codes_id = $code -> id;
                            $transactionCodes -> save();
                        }
                    }
                }

                App::flash('message', "Code successfully generated!");
                Response::redirect($this -> siteUrl('/') . 'admin/onlinepurchases?redirect=' . base64_encode($redirect));

            }
            catch(UserNotFoundException $e){
                App::flash('message', "Recipient username does not exist!");
                Response::redirect($this -> siteUrl('/') . 'admin/onlinepurchases?redirect=' . base64_encode($redirect));
            }
        }
    }
}